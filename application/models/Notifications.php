<?php
class Notifications extends CI_Model 
{
	
	public function __construct() 
	{

	}

	// Envio del email de bienvenida.
	public function welcome($id_user = FALSE)
	{
        $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
        if (empty($id_user)) return FALSE;

       	// Obtenemos los datos basicos del usuario, para armarle el email.
       	$data['user'] = $this->users->get_user($id_user);

       	$email 			= $data['user']['usr_email'];
       	$subject		= 'Bienvenido a '.SITE_NAME;
       	$content		= $this->load->view('mails/1/welcome', $data, TRUE);

       	$this->send($email, $subject, $content, 'WLCM');
	}

	// Envio del email luego de que el usuario actualiza su direccion de correo.
	public function email_updated($id_user = FALSE)
	{
        $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
        if (empty($id_user)) return FALSE;

       	// Obtenemos los datos basicos del usuario, para armarle el email.
       	$data['user'] = $this->users->get_user($id_user);

       	$email 			= $data['user']['usr_email'];
       	$subject		= 'Confirmación de correo electronico.';
       	$content		= $this->load->view('mails/1/email_updated', $data, TRUE);

       	$this->send($email, $subject, $content, 'EUPD');
	}

	// Enviamos el email con la clave temporal para resetear la cuenta
	public function new_temporal_password($id_user, $new_temporal_password)
	{
        if (empty($id_user)) return FALSE;

       	// Obtenemos los datos basicos del usuario, para armarle el email.
       	$data['user'] = $this->users->get_user($id_user);
       	$data['password'] = $new_temporal_password;

       	$email 			= $data['user']['user_email'];
       	$subject		= 'Recuperación de contraseña.';
       	$content		= $this->load->view('mails/1/new_temporal_password', $data, TRUE);

       	$this->send($email, $subject, $content, 'NPSW');
	}

	// Funcion general que canaliza el despacho de email.
  public function send($email, $subject, $content, $on_debug = 'DFLT')
  {
  	// En entorno de produccion despachamos los emails realmente
  	if (ENVIRONMENT == 'production') 
  	{
        $this->load->library('email');
        $this->email->initialize();
        $this->email->from(EMAILS_SENDER_MAIL.'@'.EMAILS_DOMAIN, EMAILS_SENDER_NAME);
        $this->email->to($email);
        $this->email->subject(mb_convert_encoding($subject, "UTF-8"));        
        $this->email->message(mb_convert_encoding($content, "UTF-8"));
        $this->email->set_mailtype("html");
        return @$this->email->send(); 
  	}
  	// En entorno de desarrollo guaradmos el email que saldria como htm en la carpeta de debuging.
  	else
  	{
  		$filename = date('YmdHis').'-'.$on_debug.'-'.$email.'.htm';
  		@file_put_contents('./devs/debug/emails/'.$filename, $content);
  	}
  }


}