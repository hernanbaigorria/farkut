<?php
class Configurations extends CI_Model 
{

    public function __construct() {}

    // GESTION DE CONFIGURACIONES
        
        // Controla que una configuration ya este seteada.
        // En caso de que no este, la setea con los valores por defecto.
        public function get($scope, $item, $reference, $lang = FALSE)
        {
            // A menos que se nos especifique un lenguaje, usamos el del usuario.
            if ($lang === FALSE) $lang = $this->session->userdata('user_language');

            // Verificamos que el lenguaje solicitado se encuentre entre los existentes.
            $available_languages = $this->config->item('available_languages');
            if (!in_array($lang, $available_languages) AND $lang !== FALSE) 
                show_error("Configuraciones: Se intenta consultar el item '$item' en un idioma no soportado: '$lang'.", 500, 'Error de GestorP');

            // Obtenemos las configuraciones por defecto.
            $default_configurations = $this->get_default_config($scope, $reference, $item);
            
            // Variantes para encontrar la configuracion. Omitimos el lenguaje en una primer instancia.
            $conditions['config_scope']       = $scope;
            $conditions['config_item']        = $item;
            $conditions['config_reference']   = (int)$reference;

            // Si el tipo de item es multilingue.
            $lang = ($default_configurations['multilang'] == TRUE) ? $lang : NULL;
            if ($lang !== NULL)
                $conditions['config_lang']   = $lang;

            // Buscamos el registro.
                      $this->db->where($conditions);
            $result = $this->db->get('configurations');

            // Si encontramos el registro..
            if ($result->num_rows() > 0)
            {
                // Normalizamos
                $result = $result->result_array();

                // Existe el registro para esta configuracion. Retornamos el valor.
                return $result[0]['config_value'];
            }
            else
            {
                $default_value = $default_configurations['default'];

                if (!is_array($default_value))
                {
                    $default_value = $default_configurations['default'];
                }
                else
                {
                    // Asignamos un valor
                    if (!isset($default_configurations['default'][$lang]) AND $lang !== NULL) 
                        $default_value = NULL;
                    else
                        $default_value = ($default_configurations['multilang'] == TRUE) ? $default_configurations['default'][$lang] : $default_configurations['default'];
                }

                // Al no existir el registro, lo configuramos por defecto.

                if (is_array($default_value)) 
                    show_error("Configuraciones: Revise el valor por defecto de '$item', no parece correcto.", 500, 'Error de GestorP');

                $result = $this->set($scope, $item, $reference, $default_value, $lang, TRUE);
                if ($result == TRUE)
                    return $default_value;

                return FALSE;
            }
        }

        // Establece una confiiguracion, o si existe la actualiza.
        public function set($scope, $item, $reference, $value, $lang = FALSE, $default_setup = FALSE)
        {
            $value = (string)trim($value);
            $default_configurations = $this->get_default_config($scope, $reference, $item);

            // El seteo es automatico?
            if ($default_setup == FALSE)
            {
                // Al no ser un seteo automatico, donde levanta por defecto, quiere decir que alguien esta guardando esta configuracion manualmente.

                // Revisamos que tenga los permisos necesarios, en caso de ser necesario.
                if ($default_configurations['permissions'] !== FALSE)
                {
                    // Vemos si se requieren varios permisos o uno solo (string o array).
                    if (is_array($default_configurations['permissions']))
                    {
                        foreach ($default_configurations['permissions'] as $key => $permission) 
                            if(!check_permissions($scope, $permission, FALSE)) return FALSE;
                            
                    }
                    else
                        if(!check_permissions($scope, $default_configurations['permissions'], FALSE)) return FALSE;
                }
            }

            // Si el tipo de item es multilingue.
            $lang = ($default_configurations['multilang'] == TRUE) ? $lang : NULL;

            // Controlamos que el lenguaje a insertar corresponda a los lenguajes instalados en el sistema.
            $available_languages = $this->config->item('available_languages');
            if (!in_array($lang, $available_languages) AND $lang !== NULL) 
                show_error("Configuraciones: Se intenta guardar el item '$item' en un idioma no soportado: '$lang'.", 500, 'Error de GestorP');

            // Existe este registro de configuracion?
            $conditions['config_scope']       = $scope;
            $conditions['config_item']        = $item;
            $conditions['config_reference']   = (int)$reference;
            $conditions['config_lang']        = $lang;

            $this->db->where($conditions);
            $result = $this->db->get('configurations');

            // Existe?!!
            if ($result->num_rows() > 0)
            {
                // si... 
                $result          = $result->result_array();
                $id_config       = $result[0]['id_config'];
                
                // entonces si es distinto, actualizamos el valor del registro.
                if ($value != $result[0]['config_value'])
                {
                    $new_config['config_value']   = $value;

                                $this->db->where('id_config', $id_config);
                    $result =   $this->db->update('configurations', $new_config);
                    
                    if ($result == TRUE)
                        return (bool)$this->db->affected_rows();
                }

                // Si el registro esta vacio, lo elimina para que no sobrescriba una configuracion heredada.
                if (strlen($value) == 0)
                {
                    $this->del($result[0]['id_config']);
                    return TRUE;
                }

                return FALSE;
            }
            else
            {
                // no... entonces lo insertamos.
                $new_configuration                     = $conditions;
                $new_configuration['config_value']     = $value;
                $new_configuration['config_date']      = date('Y-m-d H:i:s');
                $new_configuration['config_last_editor'] = $this->session->userdata('id_user');
                
                if (strlen($value) == 0) return TRUE; // No guardamos los vacios, idem anterior.

                $result =   $this->db->insert('configurations', $new_configuration);

                if ($result == TRUE)
                    return (bool)$this->db->affected_rows();

                return FALSE;
            }
        }

        public function get_configuration_records($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20)
        {
            $cond = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_config'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result = $this->db->get('configurations');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result;
            }

            return array();
        }

        public function del($id_config)
        {
            if ($id_config === FALSE) return FALSE;
            $cond['id_config'] = $id_config;

            $this->db->where($cond);
            $result = $this->db->delete('configurations');

            return (bool)$this->db->affected_rows();
        }

    // OBTENCION DE CONFIGURACIONES

        // Retorna las configuraciones SETEADAS para un determinado scope y reference, en un idioma especifico.
        public function get_setted_configurations($scope, $reference, $lang)
        {
            $conditions_localized['config_scope']       = $scope;
            $conditions_localized['config_reference']   = $reference;
            $conditions_localized['config_lang']        = $lang;

            $conditions_unloacalized['config_scope']       = $scope;
            $conditions_unloacalized['config_reference']   = $reference;
            $conditions_unloacalized['config_lang']        = NULL;

                      $this->db->where($conditions_localized);
                      $this->db->or_group_start();
                        $this->db->where($conditions_unloacalized);
                      $this->db->group_end();
            $result = $this->db->get('configurations');

            if ($result->num_rows() > 0)
            {
                // Normalizamos
                $result = $result->result_array();
                return $result;
            }

            return array();
        }

        // Retorna el ARBOL DE CONFIGURACIONES, con los valores de configuracion SETEADOS.
        public function get_structure_and_setted_configurations($scope, $config_reference, $lang)
        {
            // Cargamos en memoria la estructura dde configuracion de este scope.
            $entity_name    = $this->configurations->load_config_structure($scope, $config_reference);
            // Obtenemos todas las configuraciones.
            $configurations = $this->config->item('configurations');
            
            $config_scope   = $configurations[$scope][$entity_name];

            // Obtenemos las configuraciones guardadas para este scope y reference.
            $config_saved   = $this->configurations->get_setted_configurations($scope, $config_reference, $lang);

            foreach ($config_saved as $key => $item) 
            {
                $config_item = $item['config_item'];
                if (isset($config_scope[$config_item])) 
                    $config_scope[$config_item]['setted'] = $item;
            }

            return $config_scope;
        }

        // Retorna los elementos y valores de configuracion, PROCESADOS para utilizar en el renderizado del sitio.
        public function get_configurations($scope, $reference, $lang)
        {
            $configurations = $this->get_structure_and_setted_configurations($scope, $reference, $lang);

            $clean          = array();
            if (is_array($configurations))
                foreach ($configurations as $key => $cfg) 
                {
                    // Obtenemos el valor de la configuracion
                    switch ($cfg['type']) 
                    {
                        case 'image':
                            if (isset($cfg['setted']['config_value'])) 
                                $value = $this->collections->get_collection_items($cfg['setted']['config_value']);
                            else
                                $value = NULL;
                            break;
                        case 'video':
                            if (isset($cfg['setted']['config_value'])) 
                                $value = $this->collections->get_collection_items($cfg['setted']['config_value']);
                            else
                                $value = NULL;
                            break;
                        case 'pdf':
                            if (isset($cfg['setted']['config_value'])) 
                                $value = $this->collections->get_collection_items($cfg['setted']['config_value']);
                            else
                                $value = NULL;
                            break;
                        case 'page':
                            if (isset($cfg['setted']['config_value'])) 
                                $value = $this->collections->get_collection_items($cfg['setted']['config_value']);
                            else
                                $value = NULL;
                            break;
                        default:
                            if (isset($cfg['setted']['config_value'])) 
                                $value = $cfg['setted']['config_value'];
                            else
                                $value = NULL;
                            break;
                    }
                    $clean[$key] = $value;
                    
                }
            return $clean;
        }

    // HELPERS

        // Devuelve la informacion de un item de configuracion.
        // En caso de que el item consultado no exista, da error.
        private function get_default_config($scope, $reference, $item)
        {
            $entity_name            = $this->load_config_structure($scope, $reference);
            $default_configurations = $this->config->item('configurations');
            
            if (!isset($default_configurations[$scope][$entity_name][$item])) 
                show_error("Configuraciones: El item '$item' no existe en '$scope', o '$scope' no existe.", 500, 'Error de GestorP');

            return $default_configurations[$scope][$entity_name][$item];
        }

        // Carga al request la estructura de configuracion de la entidad requerida,
        // RETORNA: el nombre de la entidad asociada al reference. (para un scope Component, retornaria el th_component, que es el nombre de componente)
        // Los scopes hasta ahora son: site, page, component.
        public function load_config_structure($scope, $reference = FALSE)
        {
            // Obtiene la configuracion general del Site, establecida en el tema en uso.
            if ($scope == 'system')
            {
                // Obtenemos las opciones de configuracion de del tema
                $options = $this->load_config_file('system');

                // Regeneramos las opciones de configuracion.
                $configurations = $this->config->item('configurations');
                $configurations['system']['advanced'] = $options;
                $this->config->set_item('configurations', $configurations);
                return 'advanced';
            }

            // Obtiene la configuracion general del Site, establecida en el tema en uso.
            if ($scope == 'site')
            {
                // Obtenemos las opciones de configuracion de del tema
                $options = $this->load_config_file('site');

                // Regeneramos las opciones de configuracion.
                $configurations = $this->config->item('configurations');
                $configurations['site']['general'] = $options;
                $this->config->set_item('configurations', $configurations);
                return 'general';
            }

            // Obtiene la configuracion de una Page. Se requiere que REFERENCE sea especificado, para identificar el layout de la pagina, y cargar sus opciones.
            if ($scope == 'page')
            {
                $page       = $this->pages->get_page($reference, TRUE);
                $id_page    = $page['id_page'];
                
                // Si la pagina es interna, utiliza configuracion de acuerdo al layout
                if ($page['is_external'] == 0)
                {
                    // Obtenemos el layout perteneciente a esa page.
                    $th_layout  = ($page['id_parent_page'] == 0) ? $page['th_layout'] : $page['parent_page']['th_layout'];

                    // Obtenemos las opciones de configuracion de este layout
                    $options = $this->load_config_file('layouts', $th_layout);
                    
                    // Regeneramos las opciones de configuracion.
                    $configurations = $this->config->item('configurations');
                    $configurations['page'][$th_layout] = $options;

                    $entity_name = $th_layout;
                }

                // Si la pagina es interna, utiliza configuracion de acuerdo al layout
                if ($page['is_external'] == 1)
                {
                    // Obtenemos las opciones de configuracion de las paginas externas
                    $options = $this->load_config_file('external_links');

                    // Regeneramos las opciones de configuracion.
                    $configurations = $this->config->item('configurations');
                    $configurations['page']['external_links'] = $options;

                    $entity_name = 'external_links';
                }

                $this->config->set_item('configurations', $configurations);
                return $entity_name;
            }
            
            // Obtiene la configuracion de componentes
            if ($scope == 'component') 
            {
                // Obtenemos el component perteneciente a esa page.
                $component      = $this->components->get_component($reference);
                #$id_component   = $component['id_component'];
                $th_component   = $component['th_component'];

                // Obtenemos las opciones de configuracion de este layout
                $options = $this->load_config_file('components', $th_component);

                // Añadimos las opciones a memoria.
                $configurations = $this->config->item('configurations');
                $configurations['component'][$th_component] = $options;
                $this->config->set_item('configurations', $configurations);
                return $th_component;
            }

            // Obtiene la configuracion de componentes
            if (strpos($scope, 'module:') !== FALSE) 
            {
                // Identificamos el nombre del modulo.
                $module_name    = explode(':', $scope);
                $module_name    = $module_name[1];

                if (empty($module_name)) exit('No se especifico modulo para cargar la configuracion');
                
                // Obtenemos las opciones de configuracion de este layout
                $options = $this->load_config_file($scope);

                // Añadimos las opciones a memoria.
                $configurations = $this->config->item('configurations');
                $configurations[$scope][$module_name] = $options;
                $this->config->set_item('configurations', $configurations);
                return $module_name;
            }
        }

        // Carga en memoria la estructura de configuacion requerida, 
        // Levantando las opciones desde archivos de configuracion ubicados en las carpetas del tema.
        private function load_config_file($class, $name = FALSE)
        {
            if ($class == 'system')
                $config_schema = APPPATH.'config'.DIRECTORY_SEPARATOR.'advanced.php';

            // Si se pide la configuracion de paginas externas.  
            if ($class == 'external_links')
                $config_schema = PATH_THEME_CODE.'config_external_links.php';

            // Si se pide la configuracion general del sitio, la encontramos en la raiz de la carpeta del tema.  
            if ($class == 'site')
                $config_schema = PATH_THEME_CODE.'config_schema.php';

            // Si se pide la configuracion de un layout, la encontramos en la raiz de cada carpeta de layout.
            if ($class == 'layouts')
                $config_schema = PATH_THEME_CODE.$class.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.'config_schema.php';

            // Si se pide la configuracion de un componente, la encontramos en la raiz de cada carpeta de componente.
            if ($class == 'components')
                $config_schema = PATH_THEME_CODE.$class.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.'config_schema.php';

            // Si se pide la configuracion de un modulo, la encontramos en la raiz de cada carpeta de Modulo.
            if (strpos($class, 'module:') !== FALSE)
            {
                $module_name    = explode(':', $class);
                $module_name    = $module_name[1];

                $config_schema  = PATH_THEME_CODE.'modules'.DIRECTORY_SEPARATOR.$module_name.DIRECTORY_SEPARATOR.'config_schema.php';
            }


            // Si el archivo de configuracion existe, lo cargamos.
            if (file_exists(@$config_schema))
            {
                unset($ADDED_CFG);
                include($config_schema);
                return @$ADDED_CFG;
            }
            else
            {
                exit("No se encuentra: $config_schema");
            }
            // De lo contrario retornamos 0 opciones de configuracion.
            return array();
        }

    // UTILS

        // Elimina todas las configuraciones que haya guardadas para este scope y reference.
        public function clean_configurations($scope, $reference)
        {
            // Obtenemos las configuraciones seteadas para cada idioma.
            $languages = $this->config->item('available_languages');
            
            foreach ($languages as $key => $lang) 
            {
                $configurations = $this->get_structure_and_setted_configurations($scope, $reference, $lang);
                
                if (is_array($configurations))
                {
                    foreach ($configurations as $pos => $config) 
                    {
                        if (isset($config['setted']))
                        {
                            // Por las dudas revisamos si es una coleccion.
                            $is_collection = in_array($config['type'], array('image'));

                            if ($is_collection)
                            {
                                $this->collections->empty_collection($config['setted']['config_value']);
                                $this->collections->del_collection($config['setted']['config_value']);
                            }

                            // Ahora si eliminamos la configuracion. 
                            $this->del($config['setted']['id_config']);
                        }
                    }
                }
            }

            return TRUE;
        }

        // Clona un item de configuracion, en todos sus idiomas, y lo aplica a una referencia objetivo.
        public function clone_configuration($scope, $from_reference, $to_reference)
        {
            $languages      = $this->config->item('available_languages');
            $already_saved  = array(); # Aquí guardamos los items single lang que ya fueron guarados.

            foreach ($languages as $key => $lang) {
                $saved_cfgs = $this->configurations->get_structure_and_setted_configurations($scope, $from_reference, $lang);

                foreach ($saved_cfgs as $item_name => $item_cfg) 
                {
                    $is_single_lang     = (@$item_cfg['multilang'] != 1);
                    $is_already_saved   = (in_array($item_name, $already_saved));
                    $is_setted          = (isset($item_cfg['setted']));

                    if ((($is_single_lang AND !$is_already_saved) OR !$is_single_lang) AND $is_setted)
                    {
                        // Marcamos este como ya clonado, si es single language.
                        if ($is_single_lang) $already_saved[] = $item_name;
                        
                        // Creamos el nuevo registro de configuracion.
                        $new_item   = $item_cfg['setted'];
                        $type       = $item_cfg['type'];

                        // Eliminamos los valores de mas, que no se deben guardar en el nuevo item.
                        unset($new_item['id_config']);

                        // Ajustamos los valores de config, para el caso de colecciones.
                        switch ($type) 
                        {
                            case 'image':
                                $new_item['config_value'] = $this->collections->clone_colection($new_item['config_value']);
                                break;
                            case 'pdf':
                                $new_item['config_value'] = $this->collections->clone_colection($new_item['config_value']);
                                break;
                            case 'video':
                                $new_item['config_value'] = $this->collections->clone_colection($new_item['config_value']);
                                break;
                            case 'page':
                                $new_item['config_value'] = $this->collections->clone_colection($new_item['config_value']);
                                break;
                            default:
                                // Duplicado comun.
                                break;
                        }

                        // Ajustamos los otros valores
                        $new_item['config_reference']   = $to_reference;
                        $new_item['config_date']        = date('Y-m-d H:i:s');
                        $new_item['config_last_editor'] = $this->session->userdata('id_user');

                        $this->db->insert('configurations', $new_item);
                    }
                }
            }

            return TRUE;
        }

        // Setea la configuracion por defecto para elementos.
        public function reset_configuration($scope, $reference)
        {
            // Vaciamos las configuraciones anteriores.
            $this->clean_configurations($scope, $reference);

            // Obtenemos las configuraciones seteadas para cada idioma.
            $languages = $this->config->item('available_languages');
            
            foreach ($languages as $key => $lang) 
            {
                $configurations = $this->get_structure_and_setted_configurations($scope, $reference, $lang);
                
                // Seteamos las configuraciones por defecto.
                if (is_array($configurations))
                {
                    foreach ($configurations as $key => $cfg) 
                    {
                        // Obtenemos el valor de la configuracion
                        switch ($cfg['type']) 
                        {
                            case 'image':
                                
                                break;
                            case 'page':
                                
                                break;
                            default:
                                    if (isset($cfg['default'][$lang]) AND $cfg['multilang'] == 1 AND is_array($cfg['default']))
                                        $value = $cfg['default'][$lang];
                                    else
                                        $value = (! is_array($cfg['default'])) ? $cfg['default'] : NULL;

                                    $this->set($scope, $key, $reference, $value, $lang, FALSE);
                                break;
                        }
                    }
                }
            }
        }
}
