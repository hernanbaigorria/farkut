<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align">
		<thead>
			<tr>
				<th class="col-md-1">
					 <?php echo $this->lang->line('general_creation_date') ?>
				</th>
				<th class="col-md-4">
					 <?php echo $this->lang->line('media_group') ?>
				</th>
				<th class="col-md-1">

				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($media_groups as $key => $group): ?>
			<tr>
				<td class="col-md-1">
					 <?php echo date_to_view($group['creation_date'], '/', '-') ?><br>
				</td>
				<td class="col-md-4">
					 <?php echo $group['group_name'] ?><br>
				</td>
				<td class="col-md-1 text-center">
					<a class="btn default btn-xs red-stripe btn-edit-group" href="javascript:;" data-id_group="<?php echo $group['id_group'] ?>" data-group_name="<?php echo $group['group_name'] ?>">
						<?php echo $this->lang->line('general_edit') ?>
					</a>
					<a class="btn red btn-xs red-stripe btn-remove-media-group" href="javascript:;" data-id_group="<?php echo $group['id_group'] ?>" data-confirmation="<?php echo $this->lang->line('general_delete_sure') ?>">
						<i class="fa fa-trash-o"></i>
					</a>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>

<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-create-group" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<form role="form" id="frm-create-media-group">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo $this->lang->line('media_new_group'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label"><?php echo $this->lang->line('media_group'); ?></label>
					<input required name="group_name" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control">
				</div>
			</div>
			<div class="modal-footer">
				<form id="frm-media-picker">
					<button type="button" data-dismiss="modal" class="btn default"><span class="md-click-circle" style="height: 65px; width: 65px; top: -14.5px; left: 3.51563px;"></span><?php echo $this->lang->line('general_cancel') ?></button>
					<button type="submit" class="btn green"><?php echo $this->lang->line('general_save') ?></button>
				</form>
			</div>
		</div>
		<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- /.modal -->
<div class="modal fade bs-modal-md" id="modal-edit-group" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<form role="form" id="frm-update-media-group">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><?php echo $this->lang->line('media_edit_group'); ?></h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
					<label class="control-label"><?php echo $this->lang->line('media_group'); ?></label>
					<input required id="txt-group-name" name="group_name" type="text" placeholder="<?php echo $this->lang->line('general_write_here'); ?>" class="form-control">
				</div>
			</div>
			<div class="modal-footer">
				<form id="frm-media-picker">
					<input type="hidden" name="id_group" value="" id="txt-group-id">
					<button type="button" data-dismiss="modal" class="btn default"><span class="md-click-circle" style="height: 65px; width: 65px; top: -14.5px; left: 3.51563px;"></span><?php echo $this->lang->line('general_cancel') ?></button>
					<button type="submit" class="btn green"><?php echo $this->lang->line('general_save') ?></button>
				</form>
			</div>
		</div>
		<!-- /.modal-content -->
		</form>
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->