        <?php if (check_permissions('gestorp', 'upload_files')): ?>
        <form class="innerT" id="fileupload" action="<?php echo base_url() ?>Ajax/Ajax_media/upload_files" method="POST" enctype="multipart/form-data">
            <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
            <div class="row fileupload-buttonbar innerTB">
                <div class="col-lg-7">
                    <span class="gbtn gbtn-default fileinput-button">
                        <i class="fa fa-search"></i>
                        <span><?php echo $this->lang->line('media_select_files'); ?></span>
                        <input type="file" name="files[]" multiple="">
                    </span>
                    <button type="submit" class="gbtn gbtn-success start hidden" id="btn-start-upload">
                        <i class="fa fa-upload"></i>
                        <span><?php echo $this->lang->line('media_do_upload'); ?></span>
                    </button>
                    <span class="fileupload-process">
                    </span>
                </div>
                <!-- The global progress information -->
                <div class="col-lg-5 fileupload-progress fade">
                    <!-- The global progress bar -->
                    <div class="progress progress-striped active margin-bottom-none" role="progressbar" aria-valuemin="0" aria-valuemax="100">
                        <div class="progress-bar progress-bar-success" style="width:0%;">
                        </div>
                    </div>
                    <!-- The extended global progress information -->
                    <div class="progress-extended">
                    </div>
                </div>
            </div>
            <!-- The table listing the files available for upload/download -->
            <table role="presentation" class="table table-striped table-bordered table-advance table-hover table-vertical-align margin-none innerT">
                <tbody class="files">
                </tbody>
            </table>
            <div class="innerB"></div>
        </form>
        <?php endif ?>

        <?php if (!isset($selecteds)) $selecteds = NULL; ?>
        <div class="row innerB">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
                <input class="form-control gtp-form-control" id="media_gallery_name_filter" name="media_gallery_name_filter" placeholder="<?php echo $this->lang->line('general_search'); ?>">
            </div>
            <div class="col-md-4">
                <select id="media_gallery_group_filter" name="media_gallery_group_filter" class="form-control gtp-form-control">
                        <option value="0"><?php echo $this->lang->line('media_all_groups'); ?></option>
                    <?php foreach ($media_groups as $g_key => $group): ?>
                        <option value="<?php echo $group['id_group'] ?>"><?php echo $group['group_name'] ?></option>
                    <?php endforeach ?>
                </select>
            </div>
        </div>

        <div class="row media-gallery" data-pickable="<?php echo (int)$pickable ?>" data-selected="<?php echo $selecteds ?>" data-filter="<?php echo $type_filter ?>">
            <?php $SELECCIONADOS = explode(',', $selecteds); ?>
            <?php foreach ($media_items as $key => $item): ?>
                <?php 
                    $item['selected'] = (in_array($item['id_media'], $SELECCIONADOS));
                 ?>
                <?php if ($pickable): ?>
                    <?php load_page_piece('media_item_pickable', $item); ?>
                <?php else: ?>
                    <?php load_page_piece('media_item', $item); ?>
                <?php endif ?>
            <?php endforeach ?>
        </div>

        <div class="row innerT inner-2x">
            <div class="col-md-12 text-center">
                <a class="btn_load_more_media_items btn btn-default <?php if (count($media_items) < PAGINATION_MEDIA_LIST) echo 'hidden' ?>" href="javascript:;"><?php echo $this->lang->line('general_load_more') ?></a>
            </div>
        </div>

        <?php if (check_permissions('gestorp', 'upload_files')): ?>
        <script id="template-upload" type="text/x-tmpl">
        {% for (var i=0, file; file=o.files[i]; i++) { %}
            <tr class="template-upload fade">
                <td class="col-xs-2 col-md-1">
                    <span class="preview"></span>
                </td>
                <td>
                    <p class="name margin-none">{%=file.name%}</p>
                    <strong class="error text-danger label label-danger"></strong>
                </td>
                <td>
                    <div class="group">    
                        <select name="group[]" class="form-control">
                                <option value="0"><?php echo $this->lang->line('media_without_group'); ?></option>
                            <?php foreach ($media_groups as $g_key => $group): ?>
                                <option value="<?php echo $group['id_group'] ?>"><?php echo $group['group_name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </td>    
                <td>
                    <p class="size margin-none">Processing...</p>
                    <div class="progress progress-striped margin-bottom-none active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                        <div class="progress-bar progress-bar-success" style="width:0%;"></div>
                    </div>
                </td>
                <td class="col-xs-2 col-md-1">
                    {% if (!i && !o.options.autoUpload) { %}
                        <button class="gbtn gbtn-xs gbtn-info gbtn-block start hidden" disabled>
                            <i class="fa fa-upload"></i>
                            <span>Start</span>
                        </button>
                    {% } %}
                    {% if (!i) { %}
                        <button class="gbtn gbtn-xs gbtn-danger gbtn-block cancel">
                            <i class="fa fa-ban"></i>
                            <span>Cancel</span>
                        </button>
                    {% } %}
                </td>
            </tr>
        {% } %}
        </script>
        <!-- The template to display files available for download -->
        <script id="template-download" type="text/x-tmpl">
        </script>
        <?php endif ?>