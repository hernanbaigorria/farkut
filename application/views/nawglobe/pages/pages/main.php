<div class="tabbable tabs-right">
	<ul class="nav nav-tabs">
		<li class="active">
			<a href="#tab-pages" data-toggle="tab" aria-expanded="true">
				<?php echo $this->lang->line('pages_pages') ?>
			</a>
		</li>
		<li class="">
			<a href="#tab-sections" data-toggle="tab" aria-expanded="false">
				<?php echo $this->lang->line('pages_sections') ?>
			</a>
		</li>
		<li>
			<a href="#tab-external" data-toggle="tab">
				<?php echo $this->lang->line('pages_external_pages') ?>
			</a>
		</li>
	</ul>
	<div class="tab-content">
		<div class="tab-pane fade" id="tab-sections">
			
			<?php 
				// Filtramos las paginas sobre las cuales no tenemos permisos de edicion.
				foreach ($sections as $key => $section) {
					foreach ($section['childs'] as $skey => $child) {
						if (!check_permissions('pages', $child['id_page'])) {
							unset($sections[$key]['childs'][$skey]);
						}
					}
				}
			?>

			<?php foreach ($sections as $key => $section): ?>
				<?php if (check_permissions('pages', $section['id_page']) OR count($section['childs']) > 0): ?>
					<div class="portlet box blue-hoki marginB">
						<div class="portlet-title">
							<div class="tools pull-left">
								<a href="javascript:;" class="expand page-<?php echo $section['id_page'] ?>" data-original-title="" title=""></a>
							</div>
							<div class="caption cursor-pointer" onclick="$('.expand.page-<?php echo $section['id_page'] ?>').click();">
								&nbsp;&nbsp;<?php echo $section['page_title'] ?> 
							</div>
						</div>
						<div class="portlet-body" style="display: none;">

							<?php load_page_piece('table_section_home', array('page' => $section))  ?>

							<?php load_page_piece('table_pages', array('pages' => $section['childs'], 'parent_slug' => $section['page_slug'].'/'))  ?>
							
							<?php if (count($section['childs']) == 0): ?>
								<p class="text-center">
									<a class="btn btn-xs btn-success" href="?action=new&preset=grouped&parent=<?php echo $section['id_page'] ?>">
										<?php echo $this->lang->line('pages_create_subpage') ?>
									</a>
									<a class="btn btn-xs red red-stripe btn-remove-page" href="javascript:;" data-id_page="<?php echo $section['id_page'] ?>" data-confirmation="<?php echo $this->lang->line('pages_delete_sure') ?>">
										<?php echo $this->lang->line('pages_delete_section') ?>
									</a>
								</p>
							<?php endif ?>

						</div>
					</div>
				<?php endif ?>
			<?php endforeach ?>
		</div>
		<div class="tab-pane active in" id="tab-pages">
			<?php load_page_piece('table_pages', array('pages' => $pages, 'parent_slug' => NULL))  ?>
		</div>
		<div class="tab-pane fade" id="tab-external">
			<?php load_page_piece('table_pages', array('pages' => $externals, 'parent_slug' => NULL))  ?>
		</div>
	</div>
</div>

