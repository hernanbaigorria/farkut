<div class="row profile-account">
	<div class="col-md-3">
		<ul class="ver-inline-menu tabbable margin-bottom-10">
			<?php $permission_types = array_keys($permissions); ?>
			<?php foreach ($permission_types as $key => $permission_type): ?>
			<li class="<?php if ($key == 0) echo 'active' ?>">
				<a data-toggle="tab" href="#tab_type_<?php echo $permission_type; ?>" aria-expanded="<?php if ($key == 0) echo 'true' ?>" >
					<i class="fa fa-lock"></i> 
					<?php echo $this->lang->line('permission_type_'.$permission_type); ?> 
				</a>
				<span class="after"></span>
			</li>
			<?php endforeach ?>
		</ul>
	</div>
	<div class="col-md-9">
		<div class="tab-content">
			<?php $pos = 0; ?>
			<?php foreach ($permissions as $permission_type => $permission_items): ?>
				<div id="tab_type_<?php echo $permission_type; ?>" class="tab-pane <?php if ($pos == 0) echo 'active' ?>">
					<form role="form" class="frm_update_permissions">
						<input type="hidden" name="permission_group" value="<?php echo $permission_type; ?>">
						<input type="hidden" name="id_user" value="<?php echo $user['id_user']; ?>">
						
						<?php if ($permission_type == 'pages'): ?>
								<?php $selected = array(); ?>

								 <?php foreach ($permission_items as $item => $value): ?>
									 <?php if ($value['value'] == '1'): ?>
									 	<?php array_push($selected, $item); ?>
									 <?php endif ?>
									 <input type="hidden" value="1" name="<?php echo $item ?>">
								 <?php endforeach ?>


								<div class="innerB">
									<input class="form-control" placeholder="<?php echo $this->lang->line('general_filter'); ?>" id="page_tree_filter">
								</div>

								<div id="pages_tree"  data-selecteds="<?php echo implode(',', $selected) ?>"></div>

								<script type="text/javascript">
									jQuery(document).ready(function() {    
										start_page_picker_tree();
									});
								</script>
								<input id="page_permissions_selector" type="hidden" value="" name="page_permissions_selector">
							<?php else: ?>

								<div class="form-group form-md-checkboxes">
									<div class="md-checkbox-list">
										<?php foreach ($permission_items as $item => $value): ?>
										<?php if (is_array($value)): ?>
											<?php // En algunos casos puede ser compuesto. Como esto se utiliza solo para el formulario ?>
											<?php $label = $value['label'] ?>
											<?php $value = $value['value'] ?>
										<?php endif ?>
										<div class="md-checkbox">
											<input type="hidden" value="0" name="<?php echo $item ?>">
											<input type="checkbox" value="1" name="<?php echo $item ?>" id="chk_<?php echo $item ?>" class="md-check" <?php if ($value == TRUE) echo 'checked=""' ?>>
											<label for="chk_<?php echo $item ?>">
											<span></span>
											<span class="check"></span>
											<span class="box"></span>
											<?php if ($permission_type == 'gestorp'): ?>
												<?php echo $this->lang->line('permission_'.$item); ?>  </label>
											<?php endif ?>
											<?php if ($permission_type == 'pages'): ?>
												<?php echo $label ?>  </label>
											<?php endif ?>
											<?php if ($permission_type == 'modules'): ?>
												<?php echo ucfirst(implode(' ', explode('_', $item))) ?>  </label>
											<?php endif ?>
											<?php if (!in_array($permission_type, array('gestorp', 'pages', 'modules'))): ?>
												<?php echo $this->lang->line('permission_'.$item); ?>  </label>
											<?php endif ?>
										</div>
										<?php endforeach ?>
									</div>
								</div>

						<?php endif ?>

						
						<div class="margin-top-10">
							<button type="submit" class="btn green">
								<?php echo $this->lang->line('general_save'); ?> 
							</button>
						</div>
					</form>
				</div>
			<?php $pos++; ?>
			<?php endforeach ?>
		</div>
	</div>
</div>