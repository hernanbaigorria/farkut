<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
	<script src="../../assets/plugins/respond.min.js"></script>
	<script src="../../assets/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo nwg_assets('plugins/jquery.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/jquery-migrate.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/bootstrap/js/bootstrap.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/jquery.blockui.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/uniform/jquery.uniform.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/jquery.cokie.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/bootstrap-growl/jquery.bootstrap-growl.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('pages/js/ajax_engine.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('pages/js/common.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/jquery-inputmask/jquery.inputmask.bundle.min.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('plugins/jquery-minicolors/jquery.minicolors.min.js') ?>" type="text/javascript"></script>

<!-- END CORE PLUGINS -->

<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo nwg_assets('plugins/jquery-validation/js/jquery.validate.min.js') ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

	<?php if ($RESOURCES !== FALSE): ?>
	<!-- BEGIN: Page level plugins -->
		<?php foreach ($RESOURCES['footer_js'] as $file => $resource): ?>
			<?php if (is_array($resource)): ?> 
				<?php if (in_array(SUBPAGE, $resource['subpages'])): ?>
					<?php if (strpos($resource['link'], 'http://') === FALSE): ?>
						<script src="<?php echo nwg_assets($resource['link']) ?>" type="text/javascript"></script>
					<?php else: ?>
						<script src="<?php echo $resource['link'] ?>" type="text/javascript"></script>
					<?php endif ?>
				<?php endif ?>
			<?php else: ?>
				<?php if (strpos($resource, 'http://') === FALSE): ?>
					<script src="<?php echo nwg_assets($resource) ?>" type="text/javascript"></script>
				<?php else: ?>
					<script src="<?php echo $resource ?>" type="text/javascript"></script>
				<?php endif ?>
			<?php endif ?>
		<?php endforeach ?>
	<!-- END: Page level plugins -->
	<?php endif ?>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo nwg_assets('layout/js/metronic.js') ?>" type="text/javascript"></script>
<script src="<?php echo nwg_assets('layout/js/layout_general.js') ?>" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script>
  jQuery(document).ready(function() {     
  Metronic.init(); // init metronic core components
  Layout.init(); // init current layout
  });
</script>
<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>