<!-- BEGIN BODY -->
<body class="page-md login">
<!-- BEGIN LOGO -->
<div class="logo">
	<a href="<?php echo base_url() ?>">
		<img src="<?php echo nwg_assets_theme('nawglobe/default_logo.png') ?>" alt="<?php echo NAWGLOBE_NAME ?>" style="max-width: 250px; max-height: 200px;"/>
	</a>
</div>
<!-- END LOGO -->
<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
<div class="menu-toggler sidebar-toggler">
</div>
<!-- END SIDEBAR TOGGLER BUTTON -->
<!-- BEGIN LOGIN -->
<div class="content">
	<?php echo @$CONTENT; ?>
</div>
<!-- END LOGIN -->
<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 2015-<?php echo date('Y') ?> &copy; <?php echo NAWGLOBE_NAME ?> v<?php echo NAWGLOBE_VERSION ?>. <?php if (strlen(GESTORP_SIGNATURE) > 0) echo ' - '.GESTORP_SIGNATURE ?>
</div>
<!-- END COPYRIGHT -->