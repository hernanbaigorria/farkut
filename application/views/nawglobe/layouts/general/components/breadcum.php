	<?php if (isset($BREADCUMB)): ?>
	<ul class="page-breadcrumb">
		<?php if (is_array($BREADCUMB)): ?>
			<?php foreach ($BREADCUMB as $key => $item): ?>
				<?php if ($key < count($BREADCUMB) - 1): ?>
					<li class="active">
						<a href="<?php echo @$item['link'] ?>">
						 	<?php echo $item['name']; ?>
						 	<i class="fa fa-angle-right"></i>
						</a>
					</li>
					<?php else: ?>
					<li class="active">
					 	<?php echo $item['name']; ?>
					</li>
				 <?php endif ?>
			<?php endforeach ?>
		<?php else: ?>
		<li class="active">
			 <?php echo $BREADCUMB; ?>
		</li>
		<?php endif ?>
	</ul>
	<?php endif ?>