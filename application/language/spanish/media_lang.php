<?php 

// Rotulos
$lang['media_media'] 					= 'Archivos mutimedia';
$lang['media_upload_new_items'] 		= 'Subir archivos';
$lang['media_select_files'] 			= 'Seleccionar archivos para subir';
$lang['media_do_upload'] 				= 'Subir ahora';
$lang['media_no_more_items'] 			= 'No hay mas items para mostrar';
$lang['media_edit_item'] 				= 'Editar archivo multimedia';
$lang['media_back_gallery']				= 'Volver a la galería';
$lang['media_uploaded']					= 'Subido en';
$lang['media_size']						= 'Tamaño de archivo';
$lang['media_group']					= 'Grupo';
$lang['media_manage_group']				= 'Administrar grupos';
$lang['media_all_groups']				= 'Todos los grupos';
$lang['media_without_group']			= 'Sin grupo';
$lang['media_new_group']				= 'Nuevo grupo';
$lang['media_edit_group']				= 'Editar grupo';
$lang['media_items_in_group']			= '~ Items';

// Campos de fomularios
$lang['media_name']						= 'Titulo del arhivo';
$lang['media_file_name']				= 'Nombre del archivo';
$lang['media_public_link']				= 'Link publico';
$lang['media_magic_link']				= 'Link corto (solo para ser usado dentro del sitio)';

// Mensajes de notificaicon
$lang['media_created_succesfully'] 		= 'Archivo creado correctamente';
$lang['media_group_created_succesfully']= 'Grupo creado correctamente';
$lang['media_removed_succesfully'] 		= 'Archivo eliminado';
$lang['media_group_removed_succesfully']= 'Grupo eliminado';
$lang['media_delete_sure'] 				= '¿Estás seguro?';
$lang['media_replace_file'] 			= 'Reemplazar archivo';
$lang['media_replace_file_type_err']    = 'No puede ser remplazado por un archivo de distinto tipo.';
$lang['media_replace_file_help'] 		= 'Puede reemplazar el archivo por uno del mismo tipo, sin necesidad de actualizar las referencias.';
