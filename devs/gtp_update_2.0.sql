-- Agrego campo para usuarios developers.
ALTER TABLE `users` 
  ADD `user_dev` BOOLEAN NOT NULL DEFAULT FALSE AFTER `user_deletion_date`;

-- Comentarios de versiones.
ALTER TABLE `page_versions` 
  ADD `version_comment` TEXT NULL AFTER `version_number`;

-- Datos de ultima modificacion de la version.
ALTER TABLE `page_versions` 
  ADD `version_last_modification` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `version_comment`, 
  ADD `version_last_modifier` INT NULL AFTER `version_last_modification`;

-- Estas dos columnas de Pages se deprectan. Pasan a trackearse en versiones.
ALTER TABLE `pages`
  DROP `page_last_publish`,
  DROP `page_last_modification`,
  DROP `page_published`;

-- Campos nuevos de Pages
ALTER TABLE `pages` 
  ADD `page_cache_time` SMALLINT NOT NULL DEFAULT '0' AFTER `th_layout`, 
  ADD `page_custom_css` LONGTEXT NULL AFTER `page_cache_time`, 
  ADD `page_custom_js` LONGTEXT NULL AFTER `page_custom_css`; 

-- Indicies para Pages
ALTER TABLE `pages` ADD INDEX(`th_layout`);
ALTER TABLE `pages` ADD INDEX(`page_slug`);
ALTER TABLE `pages` ADD INDEX(`id_parent_page`);