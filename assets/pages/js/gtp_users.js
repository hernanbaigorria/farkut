$(document).on('submit', '#frm_create_account', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_users/create_account', function(data)
	{
		if (data.cod == 1) 
		{
		};
	});
	e.preventDefault();
});
$(document).on('submit', '#frm_update_account', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_users/update_account', function(data)
	{
		if (data.cod == 1) 
		{
		};
	});
	e.preventDefault();
});
$(document).on('submit', '#frm_update_password', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_users/update_password', function(data)
	{
		if (data.cod == 1) 
		{
			$("#frm_update_password")[0].reset();
		};
	});
	e.preventDefault();
});
$(document).on('click', '.btn-remove-user', function(e)
{ 
	var params = $(this).data();

	bootbox.confirm(params.confirmation, function(result) 
	{
		if (result == true)
		{
			send_button('/Ajax/Ajax_users/remove_account/', params, function(data)
			{
				if (data.cod == 1) 
				{
					
				};
			});
		}
	});

	e.preventDefault();
});
// Cargar el modal de cambio de foto de perfil.
$(document).on('click', '.btn-change-picture', function(e)
{ 
	var element = $(this);
	var element_original = element.html();
	var params = $(this).data();

	element.html('<i class="fa fa-spinner fa-spin"></i>');
	send_button('/Ajax/Ajax_users/get_modal_picture/', params, function(data)
	{
	    if (data.cod == 1) 
	    {
			$('#modal-update-picture').remove();
			$(".page-container").append(data.ext.composer);
			$('#modal-update-picture').modal("toggle");
			element.html(element_original);
	    };
	});
	e.preventDefault();
});
// Enviar formulario de actualización de foto de perfil
$(document).on('submit', '#frm-update-profile-picture', function(e)
{
  Metronic.blockUI({
      target: '#profile-image-holder',
      animate: true
  });

  send_complex_form(this, '/Ajax/Ajax_users/update_profile_picture/', function(data)
  {
      if (data.cod == 1) {
        Metronic.unblockUI('#profile-image-holder');
      }
      else {
        Metronic.unblockUI('#profile-image-holder');
      };
  });
  e.preventDefault();
});
// Guardar Permisos
$(document).on('submit', '.frm_update_permissions', function(e)
{ 
	var items       = $('#pages_tree').jstree().get_checked();
    $("#page_permissions_selector").val(items.join());

	send_complex_form(this, '/Ajax/Ajax_users/update_permissions', function(data)
	{
		if (data.cod == 1) 
		{
		};
	});
	e.preventDefault();
});
