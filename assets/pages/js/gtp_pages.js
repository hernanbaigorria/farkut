$(document).on('submit', '#frm_create_page', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_pages/create_page', function(data)
	{
		if (data.cod == 1) 
		{
		};
	});
	e.preventDefault();
});
$(document).on('submit', '#frm_update_page', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_pages/update_page', function(data)
	{
		if (data.cod == 1) 
		{
		};
	});
	e.preventDefault();
});

$(document).on('submit', '#frm_update_custom_code', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_pages/save_custom_code', function(data)
	{
		if (data.cod == 1) 
		{
		};
	});
	e.preventDefault();
});
$(document).on('click', '.btn-remove-page', function(e)
{ 
	var params = $(this).data();

	bootbox.confirm(params.confirmation, function(result) 
	{
		if (result == true)
		{
			send_button('/Ajax/Ajax_pages/remove_page/', params, function(data)
			{
				if (data.cod == 1) 
				{
					
				};
			});
		}
	});

	e.preventDefault();
});
$(document).on('click', '.btn-publish-version', function(e)
{ 
	var params = $(this).data();

	bootbox.confirm(params.confirmation, function(result) 
	{
		if (result == true)
		{
			send_button('/Ajax/Ajax_pages/publish_version/', params, function(data)
			{
				if (data.cod == 1) 
				{
					
				};
			});
		}
	});

	e.preventDefault();
});
$(document).on('click', '.btn-remove-version', function(e)
{ 
	var params = $(this).data();

	bootbox.confirm(params.confirmation, function(result) 
	{
		if (result == true)
		{
			send_button('/Ajax/Ajax_pages/remove_version/', params, function(data)
			{
				if (data.cod == 1) 
				{
					
				};
			});
		}
	});

	e.preventDefault();
});
$(document).on('click', '.btn-create-version', function(e)
{ 
	var element = $(this);
	var element_original = element.html();
	var params = $(this).data();

	element.html('<i class="fa fa-spinner fa-spin"></i>');
	send_button('/Ajax/Ajax_pages/get_version_creator/', params, function(data)
	{
	    if (data.cod == 1) 
	    {
			$('#modal-create-version').remove();
			$(".page-container").append(data.ext.composer);
			$('#modal-create-version').modal("toggle");
			element.html(element_original);
	    };
	});
	e.preventDefault();
});
$(document).on('submit', '#frm-create-version', function(e)
{ 
	send_complex_form(this, '/Ajax/Ajax_pages/create_version', function(data)
	{
		if (data.cod == 1) 
		{
			
		};
	});
	e.preventDefault();
});

// Controla los elementos a mostrar, segun el tipo de pagina que se vaya a crear.
$(document).on('change', 'input[name=page_type]:radio', function(e)
{ 
	var elem = $(this);
	var type = elem.val();
	handle_preset_selection(type)
	e.preventDefault();
});
jQuery(document).ready(function() 
{ 
    handle_preset_selection($('input[name=page_type]:radio:checked').val());
    handle_section_selection();
});
function handle_preset_selection(type)
{
	$('.hidden-external, .hidden-simple-page, .hidden-section-page, .hidden-section').removeClass('hidden');

	if (type == 'page') {
		$('.hidden-simple-page').addClass('hidden');
	}

	if (type == 'section_page') {
		$('.hidden-section-page').addClass('hidden');
	}

	if (type == 'external_page') {
		$('.hidden-external').addClass('hidden');
	}

	if (type == 'section') {
		$('.hidden-section').addClass('hidden');
	}
}
function handle_section_selection()
{
	var elem = $('select[name=page_parent] option:selected');
	var type = elem.data('slug');

	$('#spn-section-uri').html(type);
}
// Controla los elementos a mostrar, segun el tipo de pagina que se vaya a crear.
$(document).on('change', 'select[name=page_parent]', function(e)
{ 
	handle_section_selection();
});