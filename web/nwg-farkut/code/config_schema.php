<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	// Pagina princilpal del sitio
	$ADDED_CFG['main_page']['default']								= 0;
	$ADDED_CFG['main_page']['multilang'] 							= FALSE;
	$ADDED_CFG['main_page']['permissions'] 							= FALSE;
	$ADDED_CFG['main_page']['type'] 								= 'page';
	$ADDED_CFG['main_page']['max'] 									= 1;
	

	// Pagina 404 del sitio
	$ADDED_CFG['404_page']['default']								= 0;
	$ADDED_CFG['404_page']['multilang'] 							= FALSE;
	$ADDED_CFG['404_page']['permissions'] 							= FALSE;
	$ADDED_CFG['404_page']['type'] 									= 'page';
	$ADDED_CFG['404_page']['max'] 									= 1;
	
	
	// Titulo del sitio
	$ADDED_CFG['site_title']['default'] 							= 'Iarse';
	$ADDED_CFG['site_title']['multilang'] 	 						= FALSE;
	$ADDED_CFG['site_title']['permissions'] 						= FALSE;
	$ADDED_CFG['site_title']['type'] 								= 'text';

	// Descripcion del sitio
	$ADDED_CFG['page_description']['default'] 						= 'Descripción';
	$ADDED_CFG['page_description']['multilang'] 					= FALSE;
	$ADDED_CFG['page_description']['permissions'] 					= FALSE;
	$ADDED_CFG['page_description']['type'] 							= 'textarea';

	// Site Tags
	$ADDED_CFG['page_tags']['default']								= NULL;
	$ADDED_CFG['page_tags']['multilang'] 							= FALSE;
	$ADDED_CFG['page_tags']['permissions'] 							= FALSE;
	$ADDED_CFG['page_tags']['type'] 								= 'textarea';
	
	// icono
	$ADDED_CFG['page_favicon']['default']							= NULL;
	$ADDED_CFG['page_favicon']['multilang'] 						= FALSE;
	$ADDED_CFG['page_favicon']['permissions'] 						= FALSE;
	$ADDED_CFG['page_favicon']['type'] 								= 'image';
	$ADDED_CFG['page_favicon']['max'] 								= 1;
	
	$ADDED_CFG['logo_header']['default']							= NULL;
	$ADDED_CFG['logo_header']['multilang'] 							= TRUE;
	$ADDED_CFG['logo_header']['permissions'] 						= FALSE;
	$ADDED_CFG['logo_header']['type'] 								= 'image';
	$ADDED_CFG['logo_header']['max'] 								= 1;
    
   

	// Google Analytics Code
	$ADDED_CFG['page_ganalitics']['default']						= NULL;
	$ADDED_CFG['page_ganalitics']['multilang'] 						= FALSE;
	$ADDED_CFG['page_ganalitics']['permissions'] 					= FALSE;
	$ADDED_CFG['page_ganalitics']['type'] 							= 'textarea';

	// Menu princilpal del sitio
		
		$ADDED_CFG['page_main_menu_name1']['default'] 					= 'Menu 1';
		$ADDED_CFG['page_main_menu_name1']['multilang'] 	 			= FALSE;
		$ADDED_CFG['page_main_menu_name1']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_name1']['type'] 						= 'text';
		$ADDED_CFG['page_main_menu_link1']['default']					= 0;
		$ADDED_CFG['page_main_menu_link1']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link1']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link1']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_link1']['max'] 						= 0;
		$ADDED_CFG['page_main_menu_opts1']['default']					= 0;
		$ADDED_CFG['page_main_menu_opts1']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts1']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts1']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_opts1']['max'] 						= 0;

		$ADDED_CFG['page_main_menu_name2']['default'] 					= 'Menu 2';
		$ADDED_CFG['page_main_menu_name2']['multilang'] 	 			= FALSE;
		$ADDED_CFG['page_main_menu_name2']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_name2']['type'] 						= 'text';
		$ADDED_CFG['page_main_menu_link2']['default']					= 0;
		$ADDED_CFG['page_main_menu_link2']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link2']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link2']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_link2']['max'] 						= 0;
		$ADDED_CFG['page_main_menu_opts2']['default']					= 0;
		$ADDED_CFG['page_main_menu_opts2']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts2']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts2']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_opts2']['max'] 						= 0;

		$ADDED_CFG['page_main_menu_name3']['default'] 					= 'Menu 3';
		$ADDED_CFG['page_main_menu_name3']['multilang'] 	 			= FALSE;
		$ADDED_CFG['page_main_menu_name3']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_name3']['type'] 						= 'text';
		$ADDED_CFG['page_main_menu_link3']['default']					= 0;
		$ADDED_CFG['page_main_menu_link3']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link3']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link3']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_link3']['max'] 						= 0;
		$ADDED_CFG['page_main_menu_opts3']['default']					= 0;
		$ADDED_CFG['page_main_menu_opts3']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts3']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts3']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_opts3']['max'] 						= 0;

		$ADDED_CFG['page_main_menu_name4']['default'] 					= 'Menu 4';
		$ADDED_CFG['page_main_menu_name4']['multilang'] 	 			= FALSE;
		$ADDED_CFG['page_main_menu_name4']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_name4']['type'] 						= 'text';
		$ADDED_CFG['page_main_menu_link4']['default']					= 0;
		$ADDED_CFG['page_main_menu_link4']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link4']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link4']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_link4']['max'] 						= 0;
		$ADDED_CFG['page_main_menu_opts4']['default']					= 0;
		$ADDED_CFG['page_main_menu_opts4']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts4']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts4']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_opts4']['max'] 						= 0;

		$ADDED_CFG['page_main_menu_name5']['default'] 					= 'Menu 5';
		$ADDED_CFG['page_main_menu_name5']['multilang'] 	 			= FALSE;
		$ADDED_CFG['page_main_menu_name5']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_name5']['type'] 						= 'text';
		$ADDED_CFG['page_main_menu_link5']['default']					= 0;
		$ADDED_CFG['page_main_menu_link5']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link5']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link5']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_link5']['max'] 						= 0;
		$ADDED_CFG['page_main_menu_opts5']['default']					= 0;
		$ADDED_CFG['page_main_menu_opts5']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts5']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts5']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_opts5']['max'] 						= 0;

		$ADDED_CFG['page_main_menu_name6']['default'] 					= 'Menu 6';
		$ADDED_CFG['page_main_menu_name6']['multilang'] 	 			= FALSE;
		$ADDED_CFG['page_main_menu_name6']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_name6']['type'] 						= 'text';
		$ADDED_CFG['page_main_menu_link6']['default']					= 0;
		$ADDED_CFG['page_main_menu_link6']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link6']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_link6']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_link6']['max'] 						= 0;
		$ADDED_CFG['page_main_menu_opts6']['default']					= 0;
		$ADDED_CFG['page_main_menu_opts6']['multilang'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts6']['permissions'] 				= FALSE;
		$ADDED_CFG['page_main_menu_opts6']['type'] 						= 'page';
		$ADDED_CFG['page_main_menu_opts6']['max'] 						= 0;

	// Datos complementarios

		// Direccion a mostrar
		$ADDED_CFG['general_address']['default'] 							= '';
		$ADDED_CFG['general_address']['multilang'] 	 						= FALSE;
		$ADDED_CFG['general_address']['permissions'] 						= FALSE;
		$ADDED_CFG['general_address']['type'] 								= 'page';

		// Telefono a mostrar
		$ADDED_CFG['general_phone']['default'] 								= '';
		$ADDED_CFG['general_phone']['multilang'] 	 						= FALSE;
		$ADDED_CFG['general_phone']['permissions'] 							= FALSE;
		$ADDED_CFG['general_phone']['type'] 								= 'text';

		// Email a mostrar
		$ADDED_CFG['general_mail']['default'] 								= '';
		$ADDED_CFG['general_mail']['multilang'] 	 						= FALSE;
		$ADDED_CFG['general_mail']['permissions'] 							= FALSE;
		$ADDED_CFG['general_mail']['type'] 									= 'text';

		// Email a mostrar
		$ADDED_CFG['general_mail2']['default'] 								= '';
		$ADDED_CFG['general_mail2']['multilang'] 	 						= FALSE;
		$ADDED_CFG['general_mail2']['permissions'] 							= FALSE;
		$ADDED_CFG['general_mail2']['type'] 								= 'text';

		// Página de Facebook
		$ADDED_CFG['page_contact']['default']						= 0;
		$ADDED_CFG['page_contact']['multilang'] 	 				= FALSE;
		$ADDED_CFG['page_contact']['permissions'] 					= FALSE;
		$ADDED_CFG['page_contact']['type'] 							= 'page';
		$ADDED_CFG['page_contact']['max'] 							= 1;


		// Página de Facebook
		$ADDED_CFG['page_social_facebook']['default']						= 0;
		$ADDED_CFG['page_social_facebook']['multilang'] 	 				= FALSE;
		$ADDED_CFG['page_social_facebook']['permissions'] 					= FALSE;
		$ADDED_CFG['page_social_facebook']['type'] 							= 'page';
		$ADDED_CFG['page_social_facebook']['max'] 							= 1;

		// Página de Twitter
		$ADDED_CFG['page_social_twitter']['default']						= 0;
		$ADDED_CFG['page_social_twitter']['multilang'] 	 					= FALSE;
		$ADDED_CFG['page_social_twitter']['permissions'] 					= FALSE;
		$ADDED_CFG['page_social_twitter']['type'] 							= 'page';
		$ADDED_CFG['page_social_twitter']['max'] 							= 1;

		// Página de Youtube
		$ADDED_CFG['page_social_youtube']['default']						= 0;
		$ADDED_CFG['page_social_youtube']['multilang'] 	 					= FALSE;
		$ADDED_CFG['page_social_youtube']['permissions'] 					= FALSE;
		$ADDED_CFG['page_social_youtube']['type'] 							= 'page';
		$ADDED_CFG['page_social_youtube']['max'] 							= 1;

		// Página de Instagram
		$ADDED_CFG['page_social_instagram']['default']						= 0;
		$ADDED_CFG['page_social_instagram']['multilang'] 	 				= FALSE;
		$ADDED_CFG['page_social_instagram']['permissions'] 					= FALSE;
		$ADDED_CFG['page_social_instagram']['type'] 						= 'page';
		$ADDED_CFG['page_social_instagram']['max'] 							= 1;

		

	
	
	// Footer

		$ADDED_CFG['page_foot_menu_p1']['default']					= 0;
		$ADDED_CFG['page_foot_menu_p1']['multilang'] 				= FALSE;
		$ADDED_CFG['page_foot_menu_p1']['permissions'] 				= FALSE;
		$ADDED_CFG['page_foot_menu_p1']['type'] 					= 'page';
		$ADDED_CFG['page_foot_menu_p1']['max'] 						= 0;
