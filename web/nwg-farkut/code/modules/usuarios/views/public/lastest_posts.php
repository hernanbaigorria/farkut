<?php #debugger($entries) ?>

            <div class="row">
                <?php foreach ($entries as $key => $_entry): ?>
                <div class="col-sm-6 m-b-mob-30">
                    <div class="blog">
                        <div class="row">
                            <div class="col-sm-12 col-lg-6">
                                <img src="<?php echo media_uri($_entry['entry_images']) ?>" style="background:url(<?php echo media_uri($_entry['entry_images']) ?>);background-size:cover;background-position:center;background-repeat:no-repeat;height:0;padding-bottom:60%;width:100%;" alt="" class="m-b-mob-30 m-b-tab-30  img-responsive">
                            </div>
                            <div class="col-sm-12 col-lg-6 padding-right-0">
                                <div class="blog-title">
                                    <?php echo ucfirst($_entry['entry_title']) ?>
                                </div>
                                <p class="hidden-sm">
                                   <?php echo text_preview($_entry['entry_short'], 100) ?>
                                </p>
                                <a href="<?php echo $_entry['url'] ?>" class="btn">LEER MÁS<i class="icon ion-arrow-right-c"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php endforeach ?>
            </div>
       