

<div class="blogs page">
<section class="page-banner second-bg">
    <div class="container">
        <div class="row">
            <h3><b>Blog</b></h3>
           
        </div>
    </div>
</section>
 <section class="blog-content">
            <div class="container">
                <div class="row">
                    <!-- ======= Posts ======= -->
                   
                    <div class="col-sm-8">
                            <div class="col-sm-12 padding-0">
                                <img src="<?php echo media_uri($entry['entry_images']) ?>" style="background:url('<?php echo media_uri($entry['entry_images']) ?>');height:0;width:100%;padding-bottom:50%;background-size:cover;background-repeat:no-repeat;background-position:center;" class="img-responsive" alt="">
                            </div>
                             <div class="blog secc-blog">
                                <div class="row">
                                    
                                    <div class="col-sm-12">
                                        <ul class="blog-meta list-inline list-unstyled">
                                            <li><a href="#"><i class="icon ion-calendar"></i><?php echo date_to_view($entry['entry_date']) ?></a></li>
                                        </ul>
                                        <a href="single.html" class="blog-title">
                                            <h4><?php echo $entry['entry_title'] ?></h4>
                                        </a>
                                        <p>
                                            <?php echo $entry['entry_content'] ?>
                                        </p>
                                        <a href="/novedades" class="btn">< Volver</a>
                                    </div>

                                    <!-- Go to www.addthis.com/dashboard to customize your tools --> 
                                    <div class="col-sm-12" style="margin-top:25px;">
                                        <div class="addthis_inline_share_toolbox"></div>
                                    </div>
                                </div>

                            </div>
                            
                        
                     
                        <!--
                        <nav class="pagination_nav m-b-mob-30">
                            <nav aria-label="..." class="paginador-deft">
                                <?php echo generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page); ?>
                            </nav>
                        </nav>
                        -->
                    </div>
                    <!-- ======= Sidebar ======= -->
                    <div class="col-sm-4 sidebar">
                        <!--Categories-->
                        <section class="categories">
                            <h5>Categorias</h5>  
                            <ul class="list-unstyled">
                                <?php foreach ($categories as $_kcategory => $_category): ?>
                                    <?php #$cant_cat = count($_category) ?>
                                <li class="">
                                    <a href="<?php echo base_url('novedades/'.$_category['category_slug']) ?>" class="cat-name"><?php echo $_category['category_name'] ?></a>
                                    
                                </li>
                                <?php endforeach ?>
                            </ul>
                        </section>
                        <!--Recent Posts-->
                        <section class="recent-posts">
                            <h5>Últimas publicaciones</h5>
                            <?php echo Modules::run('novedades/block_lastest_entries_mod'); ?>
                        </section>
                    </div>
                </div>
            </div>
        </section>



<!-- Open Graph data -->
<?php $META_TAGS = $this->load->view('public/SEO', array('entry' => $entry), TRUE); ?>
<?php define('SEO_TAGS', base64_encode($META_TAGS)) ?>