<div class="tabbable tabbable-custom tabbable-noborder ">
    
    <ul class="nav nav-tabs">
        <li>
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/productos">
                Entradas de novedades
            </a>
        </li>
        <li class="active">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/productos/categorias">
                Categorias
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content innerAll">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover table-vertical-align">
                            <thead>
                                <tr>
                                    <th class="col-md-10"> Categoría </th>
                                    <th class="col-md-2"> &nbsp; </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($categories as $key => $_category): ?>
                                <tr>
                                    <td>
                                        <?php echo $_category['category_name'] ?>
                                    </td>
                                    <td class="col-md-2 text-right">
                                        <a class="btn default btn-xs red-stripe" href="?action=edit&id=<?php echo $_category['id_category']; ?>">
                                            Editar
                                        </a>
                                        <a class="btn red btn-xs red-stripe btn-remove-category" href="javascript:;" data-id_category="<?php echo @$_category['id_category'] ?>" data-confirmation="¿Estas seguro?">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                    <?php echo generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).on('click', '.btn-remove-category', function(e)
{ 
    var params = $(this).data();

    bootbox.confirm(params.confirmation, function(result) 
    {
        if (result == true)
        {
            send_button('/productos/Ajax/del_category', params, function(data)
            {
                if (data.cod == 1) 
                {

                };
            });
        }
    });

    e.preventDefault();
});
</script>