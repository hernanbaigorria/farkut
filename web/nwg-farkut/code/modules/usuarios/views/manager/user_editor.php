<div class="portlet box blue-hoki">

    <div class="portlet-title">

        <div class="caption">
            Editar Usuario
        </div>
        
    </div>

    <div class="portlet-body">
                        
        <form id="frm_update_entry" role="form" action="javascript:;">

            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label class="control-label">Nombre</label>
                        <input required name="nombre" type="text" value="<?php echo $user['nombre'] ?>" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label class="control-label">Usuario</label>
                        <input required name="usuario" type="email" value="<?php echo $user['usuario'] ?>" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="form-group">
                        <label class="control-label">Contraseña</label>
                        <input required name="contrasenia" type="password" value="<?php echo $user['contrasenia'] ?>" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
                    </div>
                </div>
            </div>
            
            <input type="hidden" name="id_user" value="<?php echo $user['id'] ?>">
            <div class="margin-top-10">
                <a href="?action=list" class="btn default">
                    Cancelar
                </a>
                <button type="submit" class="btn green">
                    Guardar
                </button>
            </div>
        </form>

    </div>

</div>
    


<script type="text/javascript">
$(document).on('submit', '#frm_update_entry', function(e)
{ 
    send_complex_form(this, '/usuarios/Ajax/upd_user', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
</script>

<script type="text/javascript">
$(document).on('change', '#sel-category', function(e)
{ 
    var selected_slug = $(this).find(':selected').data('slug')
    $('#lbl_category_slug').html(selected_slug);
    e.preventDefault();
});
</script>