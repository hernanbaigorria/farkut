<div class="tabbable tabbable-custom tabbable-noborder ">
    
    <ul class="nav nav-tabs">
        <li class="active">
            <a href="#">
                Usuarios
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content innerAll">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover table-vertical-align">
                            <thead>
                                <tr>
                                    <th class="col-md-1"> Nombre </th>
                                    <th class="col-md-6"> Usuario </th>
                                    <th class="col-md-2 text-right">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($entries as $key => $_entry): ?>
                                <tr>
                                    <td>
                                        <?php echo $_entry['nombre'] ?>
                                    </td>
                                    <td>
                                        <?php echo $_entry['usuario'] ?>
                                    </td>
                                    <td class="col-md-2 text-right">
                                        <a class="btn default btn-xs red-stripe" href="?action=edit&id=<?php echo $_entry['id']; ?>">
                                            Editar
                                        </a>
                                        <a class="btn red btn-xs red-stripe btn-remove-entry" href="javascript:;" data-id_user="<?php echo @$_entry['id'] ?>" data-confirmation="¿Estas seguro?">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                    <?php echo generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).on('submit', '#frm_update_entry', function(e)
{ 
    send_complex_form(this, '/agenda/Ajax/upd_entry', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
$(document).on('click', '.btn-remove-entry', function(e)
{ 
    var params = $(this).data();

    bootbox.confirm(params.confirmation, function(result) 
    {
        if (result == true)
        {
            send_button('/usuarios/Ajax/del_user', params, function(data)
            {
                if (data.cod == 1) 
                {

                };
            });
        }
    });

    e.preventDefault();
});
</script>