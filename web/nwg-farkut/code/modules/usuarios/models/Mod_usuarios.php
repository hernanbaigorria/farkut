<?php 
class Mod_usuarios extends CI_Model 
{
	
	public function __construct() 
	{
		$this->db->query("SET sql_mode=(SELECT REPLACE(@@sql_mode, 'ONLY_FULL_GROUP_BY', ''));");
	}


	public function new_user($nombre, $usuario, $contrasenia)
	{
		$data['nombre'] 	= $nombre;
		$data['usuario'] 	= $usuario;
		$data['contrasenia'] 	= $contrasenia;

		$result = $this->db->insert('usuarios', $data);

        if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 
	}

	public function upd_entry($id_entry = FALSE, $id_category, $entry_date, $entry_published, $entry_title, $entry_content, $entry_images)
	{
		if ($id_entry === FALSE) return FALSE;

		$data['id_category'] = $id_category;
		$data['entry_date'] = $entry_date;
		$data['entry_published'] = $entry_published;
		$data['entry_title'] = $entry_title;
		$data['entry_content'] = $entry_content;
		$data['entry_images'] = $entry_images;

		$cond['id_entry'] = $id_entry;

		$this->db->where($cond);
		return $this->db->update('mod_contactos', $data);
	}

	public function get_user($id)
	{
		if ($id === FALSE) return FALSE;
		$cond['id'] = $id;

		$this->db->select('SQL_CALC_FOUND_ROWS usuarios.*', FALSE);
		$this->db->from('usuarios');
		$this->db->where($cond);
		$result = $this->db->get();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
	}

	public function del_user($id_user)
	{
		if ($id_user === FALSE) 
			return FALSE;
		
		$cond['id'] = $id_user;

		$this->db->where($cond);
		$result = $this->db->delete('usuarios');

		return $this->db->affected_rows();
	}

	public function get_usuarios($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $search_terms = FALSE)
	{
		$cond = array();

		$this->db->select('usuarios.*', FALSE);
		$this->db->from('usuarios');

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$cond[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$cond[$filter_column] = $filter_value;
			else
				if ($filter_column !== FALSE)
					$cond['id'] = $filter_column;

		if (count($cond) > 0)
			$this->db->where($cond);

		if (is_array($order_by))
		{
			foreach ($order_by as $order_column => $sort_order) {
				$this->db->order_by($order_column, $sort_order);
			}
		}

		if ($page !== FALSE)
		{
			$offset = $page*$page_items;
			$this->db->limit($page_items, $offset);
		}
		
		$result = $this->db->get();
		$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result;
		}

		return array();
	}

	public function set_entry($id_user, $filter_column = FALSE, $filter_value = FALSE)
	{
		$data = array();

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$data[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$data[$filter_column] = $filter_value;

		$cond['id'] = $id_user;

		$this->db->where($cond);
		$result = $this->db->update('usuarios', $data);

		return (bool)$this->db->affected_rows();
	}


	public function categorias_productos()
	{		

		$this->db->select('mod_productos_categories.*');
		$this->db->select('mod_contactos.entry_title');
		$this->db->group_by('mod_productos_categories.category_name');
		$this->db->join('mod_contactos', 'mod_contactos.id_category = mod_productos_categories.id_category', 'left outer');
		$query = $this->db->get('mod_productos_categories');
		return $query->result();
	}
}