<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_usuarios');
        $this->lang->load('general_lang', $this->session->userdata('user_language'));
    }

    // Llamados de gestion interna. No olvidar controlar la sesion y los permisos.

    	public function upd_user()
    	{
            $this->control_library->session_check();

            $id_user           = $this->input->post('id_user');
            $nombre        = $this->input->post('nombre');
            $usuario        = $this->input->post('usuario');
            $contrasenia        = md5($this->input->post('contrasenia'));

            // Realizamos el update.
            $this->mod_usuarios->set_entry($id_user, 'nombre', $nombre);
            $this->mod_usuarios->set_entry($id_user, 'usuario', $usuario);
            $this->mod_usuarios->set_entry($id_user, 'contrasenia', $contrasenia);

            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/usuarios';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Usuario actualizado exitosamente.', '1', '1', $ext);
    	}

        public function new_user()
        {
            $this->control_library->session_check();

            $nombre        = $this->input->post('nombre');
            $usuario        = $this->input->post('usuario');
            $contrasenia        = md5($this->input->post('contrasenia'));

            // Realizamos el update.
            $result = $this->mod_usuarios->new_user($nombre, $usuario, $contrasenia);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/usuarios';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Usuario creado exitosamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al guardar el usuario.', '0', '1');
        }

        public function del_user()
        {
            $this->control_library->session_check();

            $id_user = $this->input->post('id_user');

            // Realizamos el update.
            $result = $this->mod_usuarios->del_user($id_user);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/usuarios?action=list'.$result;
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Usuario eliminado correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar el usuario.', '0', '1');
        }

        public function new_category()
        {
            $this->control_library->session_check();

            $category_name        = $this->input->post('category_name');
            $category_slug        = $this->input->post('category_slug');

            // Realizamos el update.
            $result = $this->mod_productos_categories->new_category($category_name, $category_slug);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/categorias';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Categoría creada exitosamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al guardar la Categoría.', '0', '1');
        }

        public function upd_category()
        {
            $this->control_library->session_check();

            $id_category           = $this->input->post('id_category');
            $category_name         = $this->input->post('category_name');
            $category_slug         = $this->input->post('category_slug');

            // Realizamos el update.
            $this->mod_productos_categories->set_category($id_category, 'category_name', $category_name);
            $this->mod_productos_categories->set_category($id_category, 'category_slug', slug($category_slug));

            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/categorias';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Categoría guardada exitosamente.', '1', '1', $ext);
        }

        public function del_category()
        {
            $this->control_library->session_check();

            $id_category = $this->input->post('id_category');

            $cond['mod_usuarios.id_category']   = $id_category;
            $entries                                = $this->mod_usuarios->get_entries($cond, FALSE, 0, 10);
            
            if (count($entries) > 0) 
            {
                ajax_response('success', 'No se puede eliminar la categoría. Ya tiene entradas asociadas.', '0', '1');
            }

            $result = $this->mod_productos_categories->del_category($id_category);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos?action=list';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Categoría eliminada correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar la Categoría.', '0', '1');
        }

    // LLamados publicos.

        
}
