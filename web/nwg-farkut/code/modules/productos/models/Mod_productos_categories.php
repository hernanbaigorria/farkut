<?php 
class Mod_productos_categories extends CI_Model 
{
	
	public function __construct() 
	{

	}

	public function get_check_value($id_variable, $id_producto)
	{
		$this->db->where('id_variable', $id_variable);
		$this->db->where('id_producto', $id_producto);
		$query = $this->db->get('variables_productos');
		return $query->num_rows();
	}


	public function new_category($category_name, $category_slug, $imagen)
	{
		$data['category_name'] = $category_name;
		$data['category_slug'] = $category_slug;
		$data['imagen'] = $imagen;

		$result = $this->db->insert('mod_productos_categories', $data);

        if ($result == TRUE) 
            return $this->db->insert_id();
        else
            return FALSE; 
	}

	public function upd_category($id_category = FALSE, $category_name, $category_slug)
	{
		if ($id_category === FALSE) return FALSE;

		$data['category_name'] = $category_name;
		$data['category_slug'] = $category_slug;

		$cond['id_category'] = $id_category;

		$this->db->where($cond);
		return $this->db->update('mod_productos_categories', $data);
	}

	public function get_category($id_category)
	{
		if ($id_category === FALSE) return FALSE;
		$cond['id_category'] = $id_category;

		$this->db->where($cond);
		$result = $this->db->get('mod_productos_categories');

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
	}

	public function get_category_by_slug($slug)
	{
		$this->db->where('category_slug', $slug);
		$result = $this->db->get('mod_productos_categories');

		return $result->result();
	}

	public function get_variable($id_variable)
	{
		if ($id_variable === FALSE) return FALSE;
		$cond['id'] = $id_variable;

		$this->db->where($cond);
		$result = $this->db->get('variables');

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
	}

	public function get_tipo($id_tipo)
	{
		if ($id_tipo === FALSE) return FALSE;
		$cond['id'] = $id_tipo;

		$this->db->where($cond);
		$result = $this->db->get('tipos');

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
	}

	public function del_category($id_category)
	{
		if ($id_category === FALSE) return FALSE;
		$cond['id_category'] = $id_category;

		$this->db->where($cond);
		$result = $this->db->delete('mod_productos_categories');

		return $this->db->affected_rows();
	}

	public function del_tipo($del_tipo)
	{
		if ($del_tipo === FALSE) return FALSE;
		$cond['id'] = $del_tipo;

		$this->db->where($cond);
		$result = $this->db->delete('tipos');

		return $this->db->affected_rows();
	}

	public function del_variable($del_variable)
	{
		if ($del_variable === FALSE) return FALSE;
		$cond['id'] = $del_variable;

		$this->db->where($cond);
		$result = $this->db->delete('variables');

		return $this->db->affected_rows();
	}

	public function get_categories($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE)
	{
		$cond = array();

		$this->db->select('SQL_CALC_FOUND_ROWS mod_productos_categories.*', FALSE);
		$this->db->from('mod_productos_categories');

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$cond[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$cond[$filter_column] = $filter_value;
			else
				if ($filter_column !== FALSE)
					$cond['id_category'] = $filter_column;

		if (count($cond) > 0)
			$this->db->where($cond);

		if (is_array($order_by))
		{
			foreach ($order_by as $order_column => $sort_order) {
				$this->db->order_by($order_column, $sort_order);
			}
		}

		if ($page !== FALSE)
		{
			$offset = $page*$page_items;
			$this->db->limit($page_items, $offset);
		}
		
		$result = $this->db->get();
		$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
            foreach ($result as $key => $value) 
            {
				$result[$key]['total_results'] = $paginacion[0]['total_items'];
            }
			return $result;
		}

		return array();
	}

	public function get_variables($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE)
	{
		$cond = array();

		$this->db->select('SQL_CALC_FOUND_ROWS variables.*, .tipos.nombre as tipo', FALSE);
		$this->db->join('tipos', 'tipos.id = variables.id_tipo');
		$this->db->from('variables');

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$cond[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$cond[$filter_column] = $filter_value;
			else
				if ($filter_column !== FALSE)
					$cond['id'] = $filter_column;

		if (count($cond) > 0)
			$this->db->where($cond);

		if (is_array($order_by))
		{
			foreach ($order_by as $order_column => $sort_order) {
				$this->db->order_by($order_column, $sort_order);
			}
		}

		if ($page !== FALSE)
		{
			$offset = $page*$page_items;
			$this->db->limit($page_items, $offset);
		}
		
		$result = $this->db->get();
		$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
            foreach ($result as $key => $value) 
            {
				$result[$key]['total_results'] = $paginacion[0]['total_items'];
            }
			return $result;
		}

		return array();
	}

	public function get_tipos_editor()
	{
		$this->db->select('variables.*');
		$this->db->select('tipos.nombre as tipo');
		$this->db->group_by('variables.id_tipo');
		$this->db->join('tipos', 'tipos.id = variables.id_tipo');
		$query = $this->db->get('variables');
		return $query->result();
	}

	public function get_variables_by_tipo($id_tipo)
	{
		$this->db->select('variables.*');
		$this->db->order_by('variables.nombre', 'asc');
		$this->db->where('variables.id_tipo', $id_tipo);
		$query = $this->db->get('variables');
		return $query->result();
	}


	public function get_variables_by_tipo_by_category($id_tipo, $category = FALSE)
	{
		$this->db->select('variables.*');
		$this->db->order_by('variables.nombre', 'asc');
		$this->db->where('variables.id_tipo', $id_tipo);
		if(!empty($category)):
			$this->db->where('mod_productos_categories.id_category', $category);
		endif;

		$this->db->group_by('variables.nombre');

		$this->db->join('mod_productos_entries', 'mod_productos_entries.id_entry = variables_productos.id_producto');
		$this->db->join('mod_productos_categories', 'mod_productos_categories.id_category = mod_productos_entries.id_category');

		$this->db->join('variables', 'variables.id = variables_productos.id_variable');

		$query = $this->db->get('variables_productos');
		return $query->result();
	}

	public function get_tipos($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE)
	{
		$cond = array();

		$this->db->select('SQL_CALC_FOUND_ROWS tipos.*', FALSE);
		$this->db->from('tipos');

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$cond[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$cond[$filter_column] = $filter_value;
			else
				if ($filter_column !== FALSE)
					$cond['id'] = $filter_column;

		if (count($cond) > 0)
			$this->db->where($cond);

		if (is_array($order_by))
		{
			foreach ($order_by as $order_column => $sort_order) {
				$this->db->order_by($order_column, $sort_order);
			}
		}

		if ($page !== FALSE)
		{
			$offset = $page*$page_items;
			$this->db->limit($page_items, $offset);
		}
		
		$result = $this->db->get();
		$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
            foreach ($result as $key => $value) 
            {
				$result[$key]['total_results'] = $paginacion[0]['total_items'];
            }
			return $result;
		}

		return array();
	}

	public function set_category($id_category, $filter_column = FALSE, $filter_value = FALSE)
	{
		$data = array();

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				$data[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				$data[$filter_column] = $filter_value;

		$cond['id_category'] = $id_category;

		$this->db->where($cond);
		$result = $this->db->update('mod_productos_categories', $data);

		return (bool)$this->db->affected_rows();
	}
}