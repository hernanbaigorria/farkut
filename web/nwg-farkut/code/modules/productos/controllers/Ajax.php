<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('mod_productos_entries');
        $this->load->model('mod_productos_categories');
        $this->lang->load('general_lang', $this->session->userdata('user_language'));
    }

    // Llamados de gestion interna. No olvidar controlar la sesion y los permisos.

    	public function upd_entry()
    	{
            $this->control_library->session_check();

            $id_entry           = $this->input->post('id_entry');
            $id_category        = $this->input->post('id_category');
            $entry_title        = $this->input->post('entry_title');
            $entry_images       = $this->input->post('entry_images');
            $entry_content      = $this->input->post('entry_content');
            $entry_published    = $this->input->post('entry_published');

            //Borrar variables primero
            $this->db->where('id_producto', $id_entry);
            $this->db->delete('variables_productos');

            $variables = $this->input->post('variables');
            foreach($variables as $variable):
                $data = array(
                    'id_producto' => $id_entry,
                    'id_variable' => $variable,
                );
                $this->db->insert('variables_productos', $data);
            endforeach;

            $ancho_total    = $this->input->post('ancho_total');
            $ancho_util    = $this->input->post('ancho_util');

            // Realizamos el update.
            $this->mod_productos_entries->set_entry($id_entry, 'entry_title', $entry_title);
            $this->mod_productos_entries->set_entry($id_entry, 'entry_images', $entry_images);
            $this->mod_productos_entries->set_entry($id_entry, 'entry_content', $entry_content);
            $this->mod_productos_entries->set_entry($id_entry, 'id_category', $id_category);
            $this->mod_productos_entries->set_entry($id_entry, 'entry_published', $entry_published);
            $this->mod_productos_entries->set_entry($id_entry, 'ancho_total', $ancho_total);
            $this->mod_productos_entries->set_entry($id_entry, 'ancho_util', $ancho_util);

            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Entrada guardada exitosamente.', '1', '1', $ext);
    	}

        public function new_tipo()
        {

            $data = array(
                'nombre' => $this->input->post('nombre'),
                'icon' => $this->input->post('icon')
            );

            $this->db->insert('tipos', $data);

               $ext['action']          = 'redirect';
               $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/tipos';
               $ext['redirect_delay']  = '500';
               ajax_response('success', 'Tipo creado exitosamente.', '1', '1', $ext);

        }


        public function new_variable()
        {

            $data = array(
                'nombre' => $this->input->post('nombre'),
                'id_tipo' => $this->input->post('tipo')
            );

            $this->db->insert('variables', $data);

               $ext['action']          = 'redirect';
               $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/variables';
               $ext['redirect_delay']  = '500';
               ajax_response('success', 'Tipo creado exitosamente.', '1', '1', $ext);

        }

        public function new_entry()
        {
            $this->control_library->session_check();

            $variables = $this->input->post('variables');

            $id_category        = $this->input->post('id_category');
            $entry_title        = $this->input->post('entry_title');
            $entry_images       = $this->input->post('entry_images');
            $entry_content      = $this->input->post('entry_content');
            $entry_published    = $this->input->post('entry_published');

            $ancho_total    = $this->input->post('ancho_total');
            $ancho_util    = $this->input->post('ancho_util');

            // Realizamos el update.
            $result = $this->mod_productos_entries->new_entry($id_category, $entry_published, $entry_title, $entry_content, $entry_images, $ancho_total, $ancho_util, $variables);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Entrada creada exitosamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al guardar la entrada.', '0', '1');
        }

        public function del_entry()
        {
            $this->control_library->session_check();

            $id_entry = $this->input->post('id_entry');

            // Realizamos el update.
            $result = $this->mod_productos_entries->del_entry($id_entry);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos?action=list'.$result;
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Entrada eliminada correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar la entrada.', '0', '1');
        }

        public function new_category()
        {
            $this->control_library->session_check();

            $category_name        = $this->input->post('category_name');
            $category_slug        = $this->input->post('category_slug');
            $imagen        = $this->input->post('imagen');

            // Realizamos el update.
            $result = $this->mod_productos_categories->new_category($category_name, $category_slug, $imagen);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/categorias';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Categoría creada exitosamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al guardar la Categoría.', '0', '1');
        }

        public function upd_category()
        {
            $this->control_library->session_check();

            $id_category           = $this->input->post('id_category');
            $category_name         = $this->input->post('category_name');
            $category_slug         = $this->input->post('category_slug');
            $imagen         = $this->input->post('imagen');

            // Realizamos el update.
            $this->mod_productos_categories->set_category($id_category, 'category_name', $category_name);
            $this->mod_productos_categories->set_category($id_category, 'category_slug', slug($category_slug));
            $this->mod_productos_categories->set_category($id_category, 'imagen', slug($imagen));

            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/categorias';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Categoría guardada exitosamente.', '1', '1', $ext);
        }

        public function upd_tipo()
        {
            $this->control_library->session_check();

            $id_tipo           = $this->input->post('id_tipo');

            $data = array(
                'nombre' => $this->input->post('nombre'),
                'icon' => $this->input->post('icon')
            );

            $this->db->where('id', $id_tipo);
            $this->db->update('tipos', $data);

            // Realizamos el update.
            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/tipos';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Tipos actualizada exitosamente.', '1', '1', $ext);
        }


        public function upd_variable()
        {
            $this->control_library->session_check();

            $id_variable           = $this->input->post('id_variable');

            $data = array(
                'nombre' => $this->input->post('nombre'),
                'id_tipo' => $this->input->post('tipo')
            );

            $this->db->where('id', $id_variable);
            $this->db->update('variables', $data);

            // Realizamos el update.
            $ext['action']          = 'redirect';
            $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/variables';
            $ext['redirect_delay']  = '500';
            ajax_response('success', 'Variable actualizada exitosamente.', '1', '1', $ext);
        }

        public function del_category()
        {
            $this->control_library->session_check();

            $id_category = $this->input->post('id_category');

            $cond['mod_productos_entries.id_category']   = $id_category;
            $entries                                = $this->mod_productos_entries->get_entries($cond, FALSE, 0, 10);
            
            if (count($entries) > 0) 
            {
                ajax_response('success', 'No se puede eliminar la categoría. Ya tiene entradas asociadas.', '0', '1');
            }

            $result = $this->mod_productos_categories->del_category($id_category);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos?action=list';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Categoría eliminada correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar la Categoría.', '0', '1');
        }

        public function del_tipo()
        {
            $this->control_library->session_check();

            $id_tipo = $this->input->post('id');

            $result = $this->mod_productos_categories->del_tipo($id_tipo);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/tipos?action=list';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Tipo eliminado correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar la Categoría.', '0', '1');
        }

        public function del_variable()
        {
            $this->control_library->session_check();

            $id_variable = $this->input->post('id');

            $result = $this->mod_productos_categories->del_variable($id_variable);
            
            if ($result !== FALSE) 
            {
                $ext['action']          = 'redirect';
                $ext['redirect_target'] = base_url().GESTORP_MANAGER.'/modules/productos/variables?action=list';
                $ext['redirect_delay']  = '500';
                ajax_response('success', 'Variable eliminada correctamente.', '1', '1', $ext);
            }
            
            ajax_response('success', 'Ocurrio un errror al eliminar la Variable.', '0', '1');
        }

    // LLamados publicos.

        
}
