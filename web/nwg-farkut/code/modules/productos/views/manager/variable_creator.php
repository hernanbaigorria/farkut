<div class="portlet box blue-hoki">

    <div class="portlet-title">

        <div class="caption">
            Crear Variable
        </div>
        
    </div>

    <div class="portlet-body">
                        
        <form id="frm_create_category" role="form" action="javascript:;">

            <div class="form-group">
                <label class="control-label">Nombre</label>
                <input required name="nombre" type="text" value="" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
            </div>

            <div class="form-group">
                <label class="control-label">Tipo</label>
                <select class="form-control" name="tipo">
                    <option value="">Seleccionar tipo</option>
                    <?php foreach($tipos as $key => $tipo): ?>
                        <option value="<?=$tipo['id']?>"><?=$tipo['nombre']?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="margin-top-10">
                <a href="?action=list" class="btn default">
                    Cancelar
                </a>
                <button type="submit" class="btn green">
                    Guardar
                </button>
            </div>

        </form>

    </div>

</div>
    


<script type="text/javascript">
$(document).on('submit', '#frm_create_category', function(e)
{ 
    send_complex_form(this, '/productos/Ajax/new_variable', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
</script>