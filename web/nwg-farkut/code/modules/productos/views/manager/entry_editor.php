<div class="portlet box blue-hoki">

    <div class="portlet-title">

        <div class="caption">
            Editar Entrada
        </div>
        
    </div>

    <div class="portlet-body">
                        
        <form id="frm_update_entry" role="form" action="javascript:;">

            

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Título</label>
                        <input required name="entry_title" type="text" placeholder="" value="<?php echo $entry['entry_title'] ?>" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Categoria</label>
                        <select name="id_category" class="form-control" id="sel-category">
                            <?php foreach ($categories as $key => $_category): ?>
                                <option <?php if ($_category['id_category'] == $entry['id_category']) echo 'selected'; ?> data-slug="<?php echo $_category['category_slug'] ?>" value="<?php echo $_category['id_category'] ?>"><?php echo $_category['category_name'] ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
                
                <div class="col-md-4">
                    <label class="control-label">Publicado</label>
                    <select name="entry_published" class="form-control">
                        <option <?php if ($entry['entry_published'] == '0') echo 'selected'; ?> value="0">No</option>
                        <option <?php if ($entry['entry_published'] == '1') echo 'selected'; ?> value="1">Si</option>
                    </select>
                </div>

            </div>

            <div class="row">
                <div class="col-md-4">
                    <label class="control-label">Ancho total <small>cm</small></label>
                    <input name="ancho_total" type="number" value="<?php echo $entry['ancho_total'] ?>" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
                </div>

                <div class="col-md-4">
                    <label class="control-label">Ancho Útil <small>cm</small></label>
                    <input name="ancho_util" type="number" value="<?php echo $entry['ancho_util'] ?>" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
                </div>
                <?php foreach($tipos as $key => $tipo):
                    $variables = $this->mod_productos_categories->get_variables_by_tipo($tipo->id_tipo);
                 ?>
                    <div class="col-md-4">
                        <label class="control-label"><?=$tipo->tipo?></label>
                        <select name="variables[]" class="form-control">
                            <option value="">Seleccionar variable</option>
                            <?php foreach($variables as $variable): 
                                $checkSelect = $this->mod_productos_categories->get_check_value($variable->id,$entry['id_entry']);
                            ?>
                                <option value="<?=$variable->id?>" <?php if($checkSelect == 1): echo "selected"; endif; ?>><?=$variable->nombre?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endforeach; ?>
            </div>

            <div class="form-group" style="margin-top:30px">
                <label class="control-label">Imágenes</label>
                <div class="input-group">
                    <span class="input-group-btn btn-left">
                        <a class="btn btn-default green btn-pick-media" data-max="10" data-cfg="img_module_novedades" data-filter="image">
                            <?php echo $this->lang->line('general_select_images') ?>
                        </a>
                    </span>                     
                    <div class="input-group-control">
                        <input id="img_module_novedades" name="entry_images" type="hidden" class="form-control" value="<?php echo $entry['entry_images'] ?>" >
                        <?php echo generate_media_collection_preview($entry['entry_images'], "#img_module_novedades") ?>
                    </div>
                </div>
            </div>
            
            <div class="form-group">
                <label class="control-label">Contenido</label>
                <textarea name="entry_content" type="text" placeholder="" class="form-control hidden rich_editor" rows="5" ><?php echo $entry['entry_content'] ?></textarea>
            </div>
            
            <input type="hidden" name="id_entry" value="<?php echo $entry['id_entry'] ?>">
            <div class="margin-top-10">
                <a href="?action=list" class="btn default">
                    Cancelar
                </a>
                <button type="submit" class="btn green">
                    Guardar
                </button>
            </div>
        </form>

    </div>

</div>
    


<script type="text/javascript">
$(document).on('submit', '#frm_update_entry', function(e)
{ 
    send_complex_form(this, '/productos/Ajax/upd_entry', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
</script>

<script type="text/javascript">
$(document).on('change', '#sel-category', function(e)
{ 
    var selected_slug = $(this).find(':selected').data('slug')
    $('#lbl_category_slug').html(selected_slug);
    e.preventDefault();
});
</script>