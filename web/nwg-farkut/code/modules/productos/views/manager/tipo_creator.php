<div class="portlet box blue-hoki">

    <div class="portlet-title">

        <div class="caption">
            Crear Tipo
        </div>
        
    </div>

    <div class="portlet-body">
                        
        <form id="frm_create_category" role="form" action="javascript:;">

            <div class="form-group">
                <label class="control-label">Nombre</label>
                <input required name="nombre" type="text" value="" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
            </div>
            <div class="form-group">
                <label class="control-label">Nombre del icono <small style="text-decoration:underline;"><a href="https://fontawesome.com/v5/search?o=r&m=free" target="_blank">Obtener nombre del icono aqui.</a></small></label>
                <input required name="icon" type="text" value="" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
            </div>
            <div class="margin-top-10">
                <a href="?action=list" class="btn default">
                    Cancelar
                </a>
                <button type="submit" class="btn green">
                    Guardar
                </button>
            </div>

        </form>

    </div>

</div>
    


<script type="text/javascript">
$(document).on('submit', '#frm_create_category', function(e)
{ 
    send_complex_form(this, '/productos/Ajax/new_tipo', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
</script>