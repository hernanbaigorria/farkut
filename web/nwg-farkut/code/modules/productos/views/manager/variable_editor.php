<div class="portlet box blue-hoki">

    <div class="portlet-title">

        <div class="caption">
            Editar Categoría
        </div>
        
    </div>

    <div class="portlet-body">
                        
        <form id="frm_update_category" role="form" action="javascript:;">

            <div class="form-group">
                <label class="control-label">Nombre</label>
                <input required name="nombre" type="text" value="<?php echo $variable['nombre'] ?>" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
            </div>

            <div class="form-group">
                <label class="control-label">Tipo</label>
                <select class="form-control" name="tipo">
                    <option value="">Seleccionar tipo</option>
                    <?php foreach($tipos as $key => $tipo): ?>
                        <option value="<?=$tipo['id']?>" <?php if($tipo['id'] == $variable['id_tipo']): echo "selected"; endif; ?>><?=$tipo['nombre']?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="hidden" name="id_variable" value="<?php echo $variable['id'] ?>">
            <div class="margin-top-10">
                <a href="?action=list" class="btn default">
                    Cancelar
                </a>
                <button type="submit" class="btn green">
                    Guardar
                </button>
            </div>

        </form>

    </div>

</div>


<script type="text/javascript">
$(document).on('submit', '#frm_update_category', function(e)
{ 
    send_complex_form(this, '/productos/Ajax/upd_variable', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
</script>