<div class="tabbable tabbable-custom tabbable-noborder ">
    
    <ul class="nav nav-tabs">
        <li>
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/productos">
                Entradas de productos
            </a>
        </li>
        <li>
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/productos/categorias">
                Categorias
            </a>
        </li>
        <li>
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/productos/variables">
                Variables
            </a>
        </li>

        <li class="active">
            <a href="/<?php echo GESTORP_MANAGER ?>/modules/productos/tipos">
                Tipos
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="row">
            <div class="col-md-12">
                <div class="tab-content innerAll">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-advance table-hover table-vertical-align">
                            <thead>
                                <tr>
                                    <th class="col-md-6"> Nombre </th>
                                    <th class="col-md-2 text-right">
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($tipos as $key => $_tipo): ?>
                                <tr>
                                    <td>
                                        <?php echo $_tipo['nombre'] ?>
                                    </td>
                                    <td class="col-md-2 text-right">
                                        <a class="btn default btn-xs red-stripe" href="?action=edit&id=<?php echo $_tipo['id']; ?>">
                                            Editar
                                        </a>
                                        <a class="btn red btn-xs red-stripe btn-remove-category" href="javascript:;" data-id="<?php echo @$_tipo['id'] ?>" data-confirmation="¿Estas seguro?">
                                            <i class="fa fa-trash-o"></i>
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>

                    <?php echo generate_pagination($pagination_current, $pagination_total_items, $pagination_per_page); ?>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
$(document).on('click', '.btn-remove-category', function(e)
{ 
    var params = $(this).data();

    bootbox.confirm(params.confirmation, function(result) 
    {
        if (result == true)
        {
            send_button('/productos/Ajax/del_tipo', params, function(data)
            {
                if (data.cod == 1) 
                {

                };
            });
        }
    });

    e.preventDefault();
});
</script>