<div class="portlet box blue-hoki">

    <div class="portlet-title">

        <div class="caption">
            Crear Categoría
        </div>
        
    </div>

    <div class="portlet-body">
                        
        <form id="frm_create_category" role="form" action="javascript:;">

            <div class="form-group">
                <label class="control-label">Nombre</label>
                <input required name="category_name" type="text" value="" class="form-control" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
            </div>

            <div class="form-group">
                <label class="control-label">URL</label>
                <div class="input-group">
                    <span class="input-group-addon ">
                        <?php echo base_url() ?>productos/
                    </span>
                    <input name="category_slug" type="text" class="form-control input-slug" value="" placeholder="<?php echo $this->lang->line('general_write_here'); ?>">
                    <span class="input-group-addon ">
                        /url-de-la-entrada
                    </span>
                </div>
            </div>

            <div class="form-group" style="margin-top:20px;">
                <label class="control-label">Imágen</label>
                <div class="input-group">
                    <span class="input-group-btn btn-left">
                        <a class="btn btn-default green btn-pick-media" data-max="1" data-cfg="img_module_novedades" data-filter="image">
                            Seleccionar imagen
                        </a>
                    </span>                     
                    <div class="input-group-control">
                        <input id="img_module_novedades" name="imagen" type="hidden" class="form-control" value="" >
                        <?php echo generate_media_collection_preview(NULL, "#img_module_novedades") ?>
                    </div>
                </div>
            </div>

            <div class="margin-top-10">
                <a href="?action=list" class="btn default">
                    Cancelar
                </a>
                <button type="submit" class="btn green">
                    Guardar
                </button>
            </div>

        </form>

    </div>

</div>
    


<script type="text/javascript">
$(document).on('submit', '#frm_create_category', function(e)
{ 
    send_complex_form(this, '/productos/Ajax/new_category', function(data)
    {
        if (data.cod == 1) 
        {
        };
    });
    e.preventDefault();
});
</script>