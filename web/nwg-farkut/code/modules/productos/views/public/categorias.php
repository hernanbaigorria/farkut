<?php foreach($categories as $categoria): ?>
<a href="<?=base_url()?>productos/<?=$categoria->category_slug?>/">
    <div class="image-category" style="background-image: url('<?php echo media_uri($categoria->imagen) ?>');">
        <div class="filter-black"></div>
    </div>
    <div class="content-category">
        <h3><?=$categoria->category_name?></h3>
        <p>Ver más <i class="fas fa-caret-right"></i></p>
    </div>
</a>
<?php endforeach; ?>
