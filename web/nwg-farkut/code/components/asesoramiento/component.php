<section class="asesoramiento">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>¿NECESITÁS ASESORAMIENTO?</h3>
            </div>
            <div class="col-12 col-sm-2"></div>
            <div class="col-12 col-sm-4">
                <div class="card-asosa">
                    <img src="<?php echo THEME_ASSETS_URL ?>general/img/icon-05.png" class="img-fluid">
                    <?php gtp_paragraph('titulo-01', 'h4', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                    <?php gtp_html('content-as-01', 'div class="content-01"', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                    <div class="arrow-div click-content-01">
                        <div class="content-arrow">
                            <i class="fa-solid fa-chevron-down"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-4">
                <div class="card-asosa">
                    <img src="<?php echo THEME_ASSETS_URL ?>general/img/icon-06.png" class="img-fluid">
                    <?php gtp_paragraph('titulo-02', 'h4', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                    <div class="content-02">
                        <p><b>¿Cuál es la edad mínima para usarlo?</b> La edad mínima para conducir un monopatín o scooter en Argentina es
                        de 16 años.<br><br>
                        <b>¿Dónde puede circular?</b>
                        Solo pueden circular por las ciudades (calles y avenidas) a una velocidad máxima es de 30 km/h.<br>El uso del casco es obligatorio.<br><br>

                        Lee el artículo completo aquí.</p>
                    </div>

                    <div class="arrow-div click-content-02">
                        <div class="content-arrow">
                            <i class="fa-solid fa-chevron-down"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-2"></div>
        </div>
    </div>
</section>