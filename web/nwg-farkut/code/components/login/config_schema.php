<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['background_color']['default'] 				= 'fff';
	$ADDED_CFG['background_color']['multilang'] 	 		= FALSE;
	$ADDED_CFG['background_color']['permissions'] 			= FALSE;
	$ADDED_CFG['background_color']['type'] 					= 'text';
	$ADDED_CFG['background_color']['mask'] 					= 'mask_color';

	$ADDED_CFG['button_txt']['default'] 					= '';
	$ADDED_CFG['button_txt']['multilang'] 	 				= TRUE;
	$ADDED_CFG['button_txt']['permissions'] 				= FALSE;
	$ADDED_CFG['button_txt']['type'] 						= 'text';

	$ADDED_CFG['button_link']['default']					= 0;
	$ADDED_CFG['button_link']['multilang'] 					= FALSE;
	$ADDED_CFG['button_link']['permissions'] 				= FALSE;
	$ADDED_CFG['button_link']['type'] 						= 'page';
	$ADDED_CFG['button_link']['max'] 						= 1;

	$ADDED_CFG['margen_extra']['default'] 					= '1';
	$ADDED_CFG['margen_extra']['multilang'] 	 			= FALSE;
	$ADDED_CFG['margen_extra']['permissions'] 				= FALSE;
	$ADDED_CFG['margen_extra']['type'] 						= 'text';

	$ADDED_CFG['padding_top_none']['default'] 				= '0';
	$ADDED_CFG['padding_top_none']['multilang'] 	 			= FALSE;
	$ADDED_CFG['padding_top_none']['permissions'] 			= FALSE;
	$ADDED_CFG['padding_top_none']['type'] 					= 'checkbox';