<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['img_slider']['default']						= NULL;
	$ADDED_CFG['img_slider']['multilang'] 					= TRUE;
	$ADDED_CFG['img_slider']['permissions'] 				= FALSE;
	$ADDED_CFG['img_slider']['type'] 						= 'image';
	$ADDED_CFG['img_slider']['max'] 						= 0;
