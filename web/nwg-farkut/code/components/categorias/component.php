<section class="main-categorias">
    <div class="container">
        <?php if (!$EDITABLE): ?>
            <div class="row">
                <div class="categorias">
                    <?php echo Modules::run('productos/block_categorias'); ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-12 col-md-4 text-center box-icons">
                <img src="<?php echo THEME_ASSETS_URL ?>general/img/icons-04.png" class="img-fluid">
                <?php gtp_paragraph('titulo-01', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                <?php gtp_paragraph('parrafo-01', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
            <div class="col-12 col-md-4 text-center box-icons">
                <img src="<?php echo THEME_ASSETS_URL ?>general/img/icons-05.png" class="img-fluid">
                <?php gtp_paragraph('titulo-02', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                <?php gtp_paragraph('parrafo-02', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
            <div class="col-12 col-md-4 text-center box-icons">
                <img src="<?php echo THEME_ASSETS_URL ?>general/img/icons-06.png" class="img-fluid">
                <?php gtp_paragraph('titulo-03', 'h3', $ID_COMPONENT, $LANG, $EDITABLE) ?>
                <?php gtp_paragraph('parrafo-03', 'p', $ID_COMPONENT, $LANG, $EDITABLE) ?>
            </div>
        </div>
    </div>
</section>