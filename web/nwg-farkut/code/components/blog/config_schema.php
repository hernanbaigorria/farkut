<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	$ADDED_CFG['card_blog']['default']						= NULL;
	$ADDED_CFG['card_blog']['multilang'] 					= FALSE;
	$ADDED_CFG['card_blog']['permissions'] 				= FALSE;
	$ADDED_CFG['card_blog']['type'] 						= 'image';
	$ADDED_CFG['card_blog']['max'] 						= 0;