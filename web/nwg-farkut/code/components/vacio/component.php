<?php #debugger($COMPONENT_CFG); ?>
<?php 
    $margin = $COMPONENT_CFG['margen_extra'];
    switch ($margin) {
        case '1':
            $cols = 'col-md-10 offset-md-1';
            break;
        case '2':
            $cols = 'col-md-8 offset-md-2';
            break;
        case '3':
            $cols = 'col-md-6 offset-md-3';
            break;
        case '4':
            $cols = 'col-md-4 offset-md-4';
            break;
        default:
            $cols = 'col-xs-12 col-12';
            break;
    }
 ?>
<?php if ($EDITABLE): ?><div style="overflow: hidden;"><?php endif ?>
<div class="cres-block <?php if ($COMPONENT_CFG['padding_top_none']) echo 'padding-top-none' ?> " style="background-color: <?php echo $COMPONENT_CFG['background_color'] ?>;">
    <div class="container">
        <div class="row">
            <div class="<?php echo $cols ?>">
                <?php gtp_html('free-content', 'div', $ID_COMPONENT, $LANG, $EDITABLE) ?>

                <?php if (!empty($COMPONENT_CFG['button_link'])): ?>
                <br/>
                <a class="btn btn-bc-gray hover" href="<?php echo page_uri($COMPONENT_CFG['button_link']) ?>">
                    <?php echo $COMPONENT_CFG['button_txt'] ?>
                </a> 
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
<?php if ($EDITABLE): ?></div><?php endif ?>